#ifndef SCENE_MANAGER_HPP
#define SCENE_MANAGER_HPP

#include <iostream>
#include <list>
#include <string>
#include <unordered_map>

#include "MeshLoader.hpp"
#include "../app/Config.hpp"
#include "../app/TestData.hpp"
#include "../entity/Entity.hpp"
#include "../physics/RigidBody.hpp"
#include "../physics/World.hpp"
#include "../rendering/Camera.hpp"
#include "../rendering/Uniforms.hpp"
#include "../tools/GLTools.hpp"
#include "../tools/Logger.hpp"

class SceneManager{

	public:
		SceneManager() :
			timeStep(1 / 60.0f),
			iterations(10),
			idPool(0)
		{};

		~SceneManager();

		Entity* createEntity(const std::string& shaderProgramName, const std::string& meshName);
		void removeEntity(Entity* entity); // Note, entity must still be deleted externally.

		void render();
		void update(double deltaTime);
		void clear();

		void loadShader(const std::string& name, const std::string& vs, const std::string& fs);
		void loadMesh(const std::string& name, ShapeMeshType shape); // Simple pre-defined mesh
		// TODO: Implement mesh-from-file function.
		void loadMesh(const std::string& name, const std::string& filePath); // Load mesh from file.

		Uniforms& getGlobalUniforms(GLuint programID){return globalUniforms[programID];};
		Uniforms& getGlobalUniforms(const std::string& programName){return globalUniforms[shaderProgramCache[programName]];};

		const Entity* getEntity(int entityID) const;

		Camera& getCamera(){return camera;};

		World& getWorld(){return physicsWorld;};

		TestData& getTestData(){return testData;};

		float getTimeStep() const {return timeStep;};

		void setPhysicsStep(float timeStep, unsigned int iterations){
			this->timeStep = timeStep;
			this->iterations = iterations;
		};

	private:
		// Map GL program to entities, resulting in fewer program changes.
		std::unordered_map<int, std::list<Entity> > entities;

		std::unordered_map<std::string, Mesh> meshCache;
		std::unordered_map<std::string, GLuint> shaderProgramCache;

		std::unordered_map<GLuint, Uniforms> globalUniforms;

		std::unordered_map<std::string, float> userData;

		Camera camera;

		World physicsWorld;
		float timeStep;
		unsigned int iterations;

		TestData testData;

		int idPool; // Very basic, could be expanded to hold more IDs/reuse them, but this suffices for basic purposes.

};

#endif
