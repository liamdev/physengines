#include "Mesh.hpp"

Mesh::Mesh(): vaoID(0), numVertices(0){
	glGenVertexArrays(1, &vaoID);
};

Mesh::Mesh(const std::vector<float>& vertices, const std::vector<unsigned int>& indices):
	vaoID(0), numVertices(indices.size())
{
	glGenVertexArrays(1, &vaoID);

	addIndices(indices);
	addAttribute(vertices, 3);
};

void Mesh::addIndices(const std::vector<unsigned int>& indices){
	glBindVertexArray(vaoID);

	glGenBuffers(1, &indexBufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);
};

template <typename T>
void Mesh::addAttribute(const std::vector<T>& data, int numComponents){
	glBindVertexArray(vaoID);

	for(unsigned int i = 0; i < bufferDetails.size(); ++i){
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, bufferDetails[i].numComponents, GL_FLOAT, GL_FALSE, 0, nullptr);
	}

	GLuint vboID;
	glEnableVertexAttribArray(bufferDetails.size());
	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(T), &data[0], GL_STATIC_DRAW);
	// TODO: Some sort of mapping from C++ type to OpenGL type for param 3?
	glVertexAttribPointer(1, numComponents, GL_FLOAT, GL_FALSE, 0, nullptr);
	bufferDetails.push_back(VBODetails(vboID, numComponents));

	glBindVertexArray(0);
};

void Mesh::destroy(){
	glDeleteVertexArrays(1, &vaoID);
	glDeleteBuffers(1, &indexBufferID);

	for(VBODetails& vbo: bufferDetails)
		glDeleteBuffers(1, &vbo.vboID);
};

void Mesh::render() const{
	glBindVertexArray(vaoID);
	glDrawElements(GL_TRIANGLES, numVertices, GL_UNSIGNED_INT, nullptr);
};