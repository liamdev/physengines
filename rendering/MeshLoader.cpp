#include "MeshLoader.hpp"

inline void addVertex(std::vector<float>& vec, float x, float y, float z){
	vec.push_back(x);
	vec.push_back(y);
	vec.push_back(z);
};

inline void addTriangle(std::vector<unsigned int>& vec,
						unsigned int index1, unsigned int index2, unsigned int index3){
	vec.push_back(index1);
	vec.push_back(index2);
	vec.push_back(index3);
};

Mesh getShapeMesh(ShapeMeshType shape){
	switch(shape){
		case ShapeMeshType::TRIANGLE:
			return getTriangleMesh();
		case ShapeMeshType::PLANE:
			return getPlaneMesh();
		case ShapeMeshType::BOX:
			return getBoxMesh();
		default:
			return getUnitSphereMesh();
	};
};

Mesh getTriangleMesh(){
	float v[] = {
		0.0f, 0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f
	};
	std::vector<float> verts(std::begin(v), std::end(v));

	unsigned int i[] = {
		0, 1, 2
	};
	std::vector<unsigned int> indices(std::begin(i), std::end(i));

	return Mesh(verts, indices);
};

std::vector<GLfloat> getProjectedNormals(const std::vector<GLfloat>& vertices){
    std::vector<GLfloat> normals;

	for(auto iter = vertices.begin(); iter != vertices.end(); iter += 3){
		glm::vec3 normalDir = glm::normalize(glm::vec3(*iter, *(iter + 1), *(iter + 2)));
		normals.push_back(normalDir[0]);
		normals.push_back(normalDir[1]);
		normals.push_back(normalDir[2]);
	}

    return normals;
};

Mesh getUnitSphereMesh(){
    std::vector<GLfloat> vertices;

	const int points = 50;
	const float radius = 1.0f;
	const float PI = 3.14159265f;

    std::vector<std::vector<float> > rings;
    for(unsigned int i = 1; i < points; ++i){
        std::vector<float> ring;

        float height = (2 * radius / points) * i;
        float r = sqrt(2 * height * radius - height * height);

        for(float j = 0; j < 2 * PI; j += 2 * PI / points){
			ring.push_back(sin(j) * r);
			ring.push_back(radius - (2 * radius / points * i));
			ring.push_back(cos(j) * r);
        }
        rings.push_back(ring);
    }

    //Triangles between top ring and top point.
    for(unsigned int i = 0; i < points; ++i){
		vertices.push_back(0.0f);
		vertices.push_back(radius);
		vertices.push_back(0.0f);
		vertices.push_back(rings[0][i * 3]);
		vertices.push_back(rings[0][i * 3 + 1]);
		vertices.push_back(rings[0][i * 3 + 2]);
		vertices.push_back(rings[0][((i + 1) % points) * 3]);
		vertices.push_back(rings[0][((i + 1) % points) * 3 + 1]);
		vertices.push_back(rings[0][((i + 1) % points) * 3 + 2]);
    }

    //Triangles between consecutive rings.
    for(unsigned int i = 0; i < points - 2; ++i){
        for(unsigned int j = 0; j < points; ++j){
			vertices.push_back(rings[i][j * 3]);
			vertices.push_back(rings[i][j * 3 + 1]);
			vertices.push_back(rings[i][j * 3 + 2]);
			vertices.push_back(rings[i + 1][j * 3]);
			vertices.push_back(rings[i + 1][j * 3 + 1]);
			vertices.push_back(rings[i + 1][j * 3 + 2]);
			vertices.push_back(rings[i + 1][((j + 1) % points) * 3]);
			vertices.push_back(rings[i + 1][((j + 1) % points) * 3 + 1]);
			vertices.push_back(rings[i + 1][((j + 1) % points) * 3 + 2]);
			vertices.push_back(rings[i][j * 3]);
			vertices.push_back(rings[i][j * 3 + 1]);
			vertices.push_back(rings[i][j * 3 + 2]);
			vertices.push_back(rings[i + 1][((j + 1) % points) * 3]);
			vertices.push_back(rings[i + 1][((j + 1) % points) * 3 + 1]);
			vertices.push_back(rings[i + 1][((j + 1) % points) * 3 + 2]);
			vertices.push_back(rings[i][((j + 1) % points) * 3]);
			vertices.push_back(rings[i][((j + 1) % points) * 3 + 1]);
			vertices.push_back(rings[i][((j + 1) % points) * 3 + 2]);
        }
    }

    //Triangles between bottom ring and bottom point.
    for(unsigned int i = 0; i < points; ++i){
		vertices.push_back(0.0f);
		vertices.push_back(-radius);
		vertices.push_back(0.0f);
		vertices.push_back(rings[points - 2][((i + 1) % points) * 3]);
		vertices.push_back(rings[points - 2][((i + 1) % points) * 3 + 1]);
		vertices.push_back(rings[points - 2][((i + 1) % points) * 3 + 2]);
		vertices.push_back(rings[points - 2][i * 3]);
		vertices.push_back(rings[points - 2][i * 3 + 1]);
		vertices.push_back(rings[points - 2][i * 3 + 2]);
    }

	// TODO: Actually set up indices to avoid all of this horrific vertex re-use.
	std::vector<unsigned int> indices;
	for(unsigned int i = 0; i < vertices.size() / 3; ++i)
		indices.push_back(i);

	Mesh sphereMesh(vertices, indices);
	sphereMesh.addAttribute(getProjectedNormals(vertices), 3);

	return sphereMesh;
};

Mesh getPlaneMesh(){
	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<unsigned int> indices;

	for(int i = 0; i < 5; ++i){
		for(int j = 0; j < 5; ++j){
			vertices.push_back(i - 2.0f);
			vertices.push_back(0);
			vertices.push_back(j - 2.0f);

			normals.push_back(0);
			normals.push_back(1);
			normals.push_back(0);
		}
	}
	
	for(int i = 0; i < 4; ++i){
		for(int j = 0; j < 4; ++j){
			indices.push_back((5 * i) + j);
			indices.push_back((5 * i) + j + 1);
			indices.push_back((5 * (i + 1)) + j);

			indices.push_back((5 * (i + 1)) + j);
			indices.push_back((5 * i) + j + 1);
			indices.push_back((5 * (i + 1)) + j + 1);
		}
	}

	Mesh planeMesh(vertices, indices);
	planeMesh.addAttribute(normals, 3);

	return planeMesh;
};

Mesh getBoxMesh(){
	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<unsigned int> indices;

	// Side 1
	addVertex(vertices, -0.5f, 0.5f, -0.5f);
	addVertex(normals, 0, 1, 0);
	addVertex(vertices, -0.5f, 0.5f, 0.5f);	
	addVertex(normals, 0, 1, 0);
	addVertex(vertices, 0.5f, 0.5f, -0.5f);	
	addVertex(normals, 0, 1, 0);
	addVertex(vertices, 0.5f, 0.5f, 0.5f);	
	addVertex(normals, 0, 1, 0);

	// Side 2
	addVertex(vertices, -0.5f, 0.5f, -0.5f);	
	addVertex(normals, -1, 0, 0);
	addVertex(vertices, -0.5f, 0.5f, 0.5f);	
	addVertex(normals, -1, 0, 0);
	addVertex(vertices, -0.5f, -0.5f, -0.5f);	
	addVertex(normals, -1, 0, 0);
	addVertex(vertices, -0.5f, -0.5f, 0.5f);	
	addVertex(normals, -1, 0, 0);

	// Side 3
	addVertex(vertices, -0.5f, 0.5f, 0.5f);
	addVertex(normals, 0, 0, 1);
	addVertex(vertices, 0.5f, 0.5f, 0.5f);
	addVertex(normals, 0, 0, 1);
	addVertex(vertices, -0.5f, -0.5f, 0.5f);
	addVertex(normals, 0, 0, 1);
	addVertex(vertices, 0.5f, -0.5f, 0.5f);
	addVertex(normals, 0, 0, 1);

	// Side 4
	addVertex(vertices, 0.5f, 0.5f, 0.5f);
	addVertex(normals, 1, 0, 0);
	addVertex(vertices, 0.5f, 0.5f, -0.5f);
	addVertex(normals, 1, 0, 0);
	addVertex(vertices, 0.5f, -0.5f, 0.5f);
	addVertex(normals, 1, 0, 0);
	addVertex(vertices, 0.5f, -0.5f, -0.5f);
	addVertex(normals, 1, 0, 0);

	// Side 5
	addVertex(vertices, 0.5f, 0.5f, -0.5f);
	addVertex(normals, 0, 0, -1);
	addVertex(vertices, -0.5f, 0.5f, -0.5f);
	addVertex(normals, 0, 0, -1);
	addVertex(vertices, 0.5f, -0.5f, -0.5f);
	addVertex(normals, 0, 0, -1);
	addVertex(vertices, -0.5f, -0.5f, -0.5f);
	addVertex(normals, 0, 0, -1);

	// Side 6
	addVertex(vertices, -0.5f, -0.5f, 0.5f);
	addVertex(normals, 0, -1, 0);
	addVertex(vertices, 0.5f, -0.5f, 0.5f);
	addVertex(normals, 0, -1, 0);
	addVertex(vertices, -0.5f, -0.5f, -0.5f);
	addVertex(normals, 0, -1, 0);
	addVertex(vertices, 0.5f, -0.5f, -0.5f);
	addVertex(normals, 0, -1, 0);

	// Triangles
	addTriangle(indices, 0, 1, 2); // Top 1
	addTriangle(indices, 2, 1, 3); // Top 2
	addTriangle(indices, 4, 6, 5); // Left 1
	addTriangle(indices, 5, 6, 7); // Left 2
	addTriangle(indices, 8, 10, 9); // Front 1
	addTriangle(indices, 9, 10, 11); // Front 2
	addTriangle(indices, 12, 14, 13); // Right 1
	addTriangle(indices, 13, 14, 15); // Right 2
	addTriangle(indices, 16, 18, 17); // Back 1
	addTriangle(indices, 17, 18, 19); // Back 2
	addTriangle(indices, 20, 22, 21); // Bottom 1
	addTriangle(indices, 21, 22, 23); // Bottom 2


	Mesh planeMesh(vertices, indices);
	planeMesh.addAttribute(normals, 3);
	return planeMesh;
};