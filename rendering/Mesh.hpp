#ifndef MESH_HPP
#define MESH_HPP

#include <vector>

#include <GL/glew.h>

class VBODetails{

	public:
		VBODetails(GLuint vboID, int numComponents) :
			vboID(vboID),
			numComponents(numComponents)
		{};

		GLuint vboID;
		int numComponents;
};

class Mesh{

	public:
		Mesh();
		Mesh(const std::vector<float>& vertices, const std::vector<unsigned int>& indices);

		// Verts, normals etc.
		template <typename T>
		void addAttribute(const std::vector<T>& intData, int numComponents);

		void addIndices(const std::vector<unsigned int>& indices);

		// Explicit, avoids accidental deletion of buffers if the object is copied.
		void destroy();

		void render() const;

		bool isEmpty() const {return vaoID == 0 || bufferDetails.size() == 0;};

	private:
		GLuint vaoID;
		GLuint indexBufferID;
		std::vector<VBODetails> bufferDetails;

		GLuint numVertices;

};

#endif