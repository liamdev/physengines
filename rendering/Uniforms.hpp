#ifndef UNIFORMS_HPP
#define UNIFORMS_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "../tools/Logger.hpp"

class Uniforms{

	public:
		Uniforms():
			programID(0)
		{};
		Uniforms(GLuint programID):
			programID(programID)
		{};

		void activate();

		void addIntUniform(const std::string& uniformName, int val);
		void addFloatUniform(const std::string& uniformName, float val);
		void addVec3Uniform(const std::string& uniformName, const glm::vec3& val);
		void addMat4Uniform(const std::string& uniformName, const glm::mat4& val);

		GLuint getProgramID() const {return programID;};
		void setProgramID(GLuint programID){this->programID = programID;};

	private:
		void cacheUniformLocation(const std::string& uniformName);

		GLuint programID;

		// Uniform locations: uniform name -> location
		std::unordered_map<std::string, GLint> uniformLocations;

		// Uniform maps: uniform name -> value
		std::unordered_map<std::string, int> intUniforms;
		std::unordered_map<std::string, float> floatUniforms;
		std::unordered_map<std::string, glm::vec3> vec3Uniforms;
		std::unordered_map<std::string, glm::mat4> mat4Uniforms;

};

#endif