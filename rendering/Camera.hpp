#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../app/Config.hpp"
#include "../input/InputHandler.hpp"

class Camera{

	public:
		Camera(glm::vec3 position = glm::vec3(0, 0, -1),
				glm::vec3 lookDir = glm::vec3(0, 0, 1));

		void setPositionRotation(glm::vec3 position, glm::vec3 lookAt);
		void translate(glm::vec3 translation);

		glm::mat4 getVPMatrix() const {return vpMatrix;};
		glm::mat4 getViewMatrix() const {return view;};
		glm::mat4 getProjMatrix() const {return projection;};

		void update(double deltaTime);

	private:
		void calculateVPMatrix();

		glm::mat4 vpMatrix;

		glm::mat4 view;
		glm::mat4 projection;

		glm::vec3 pos;
		glm::vec3 lookDir;
		glm::vec3 up;

};

#endif