#include "Camera.hpp"

// Converts x,z look direction to an angle around the y axis.
float getAngle(float x, float z){
	//Prevent divide-by-zero errors.
	if(fabs(z - 0.0f) < 0.001f){
		if(x > 0)
			return Maths::PI / 2;
		else if(x < 0)
			return Maths::PI / 2;
		else
			return 0;
	}

	return -atan(x / z) + ((z < 0) ? 0 : Maths::PI);
};

Camera::Camera(glm::vec3 position, glm::vec3 lookDir) : pos(position), lookDir(lookDir){
	projection = glm::perspective(CameraParams::FOV_DEG, CameraParams::ASPECT_RATIO, CameraParams::Z_NEAR, CameraParams::Z_FAR);

	up = glm::vec3(0, 1, 0);

	// Cache VP matrix, only update if movement occurs.
	calculateVPMatrix();
};

// Set camera's position in world coordinates, and rotation by specifying a point in world
// coordinates to look at.
void Camera::setPositionRotation(glm::vec3 position, glm::vec3 lookAt){
	pos = position;
	lookDir = lookAt;

	calculateVPMatrix();
};

void Camera::translate(glm::vec3 translation){
	pos += translation;
	calculateVPMatrix();
};

// Calculate and cache VP matrix, saves recalculation at each frame.
void Camera::calculateVPMatrix(){
	view = glm::lookAt(pos, pos + lookDir, glm::vec3(0, 1, 0));
	vpMatrix = projection * view;
};

void Camera::update(double deltaTime){
	float dt = (float)deltaTime;
	InputHandler& input = Input::getHandler();

	// Point in world coordinates denoting the new camera position.
	glm::vec3 destPos = pos;

	float boostMultiplier = (input.isKeyDown(GLFW_KEY_LEFT_SHIFT)) ? CameraParams::BOOST_MULTIPLIER : 1;

	// Ascend/descend handling
	if(input.isKeyDown(GLFW_KEY_SPACE))
		destPos.y += boostMultiplier * dt;
	if(input.isKeyDown(GLFW_KEY_LEFT_CONTROL))
		destPos.y -= boostMultiplier * dt;

	// Get a vector containing the movement requested in each direction.
	glm::vec3 movement(0, 0, 0);

	if(input.isKeyDown(GLFW_KEY_W) || input.isKeyDown(GLFW_KEY_UP)){
		movement.z += 1;		
	}
	if(input.isKeyDown(GLFW_KEY_S) || input.isKeyDown(GLFW_KEY_DOWN)){
		movement.z -= 1;
	}
	if(input.isKeyDown(GLFW_KEY_A) || input.isKeyDown(GLFW_KEY_LEFT)){
		movement.x -= 1;
	}
	if(input.isKeyDown(GLFW_KEY_D) || input.isKeyDown(GLFW_KEY_RIGHT)){
		movement.x += 1;
	}

	// Only update 
	float squaredSize = movement.x * movement.x + movement.z * movement.z;
	if(squaredSize != 0){
		movement = glm::normalize(movement);

		float horizontalAngle = getAngle(lookDir.x, lookDir.z) + Maths::PI / 2;
		glm::vec3 right(sin(horizontalAngle), 0, -cos(horizontalAngle));

		destPos.x += (right.x * movement.x + lookDir.x * movement.z) * boostMultiplier * dt;
		destPos.z += (lookDir.z * movement.z + lookDir.x * movement.x) * boostMultiplier * dt;
		destPos.y += lookDir.y * movement.z * boostMultiplier * dt;
	}

	bool rotated = false;

	std::pair<double, double> mouseMove = input.getMouseMovement();
	if(mouseMove.first != 0 || mouseMove.second != 0){
		float horizontalAngle = (float)(getAngle(lookDir.x, lookDir.z) + mouseMove.first * dt);
		float verticalAngle = (float)(acos(lookDir.y) + mouseMove.second * dt);

		lookDir = glm::normalize(glm::vec3(sin(horizontalAngle), 0, -cos(horizontalAngle)));
		lookDir.y = cos(verticalAngle);
		rotated = true;
	}

	// Only update the VP matrix if the camera moves or rotates.
	if(rotated || destPos != pos){
		pos = destPos;
		calculateVPMatrix();
	}
};
