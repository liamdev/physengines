#include "Uniforms.hpp"

void logUniformCacheMiss(const std::string& uniformName, GLuint programID){
	std::stringstream ss;
	//ss << "Unable to locate uniform:\nUniform name: " << uniformName << "\nProgram ID: " << programID << "\n";
	//std::cerr << ss.str();
	Logger::warning(ss.str());
}

void Uniforms::cacheUniformLocation(const std::string& uniformName){
	if(uniformLocations[uniformName] > 0)
		return;

	if(uniformLocations[uniformName] < 0){
		logUniformCacheMiss(uniformName, programID);
		return;
	}

	GLint location = glGetUniformLocation(programID, uniformName.c_str());

	if(location < 0){
		logUniformCacheMiss(uniformName, programID);
		return;
	}

	uniformLocations[uniformName] = location;
};

void Uniforms::addIntUniform(const std::string& uniformName, int val){
	cacheUniformLocation(uniformName);
	intUniforms[uniformName] = val;
};

void Uniforms::addFloatUniform(const std::string& uniformName, float val){
	cacheUniformLocation(uniformName);
	floatUniforms[uniformName] = val;
};

void Uniforms::addVec3Uniform(const std::string& uniformName, const glm::vec3& val){
	cacheUniformLocation(uniformName);
	vec3Uniforms[uniformName] = val;
};

void Uniforms::addMat4Uniform(const std::string& uniformName, const glm::mat4& val){
	cacheUniformLocation(uniformName);
	mat4Uniforms[uniformName] = val;
};

void Uniforms::activate(){
	for(auto& uniform: intUniforms){
		glUniform1i(uniformLocations[uniform.first], uniform.second);
	}

	for(auto& uniform: floatUniforms){
		glUniform1f(uniformLocations[uniform.first], uniform.second);
	}

	for(auto& uniform: vec3Uniforms){
		glUniform3fv(uniformLocations[uniform.first], 1, &(uniform.second[0]));
	}

	for(auto& uniform: mat4Uniforms){
		glUniformMatrix4fv(uniformLocations[uniform.first], 1, GL_FALSE, &(uniform.second[0][0]));
	}
};
