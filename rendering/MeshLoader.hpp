#ifndef MESH_LOADER_HPP
#define MESH_LOADER_HPP

#include <string>

#include <glm/glm.hpp>

#include "Mesh.hpp"

enum class ShapeMeshType {TRIANGLE, SPHERE, PLANE, BOX};

Mesh getShapeMesh(ShapeMeshType shape);
Mesh getTriangleMesh();
Mesh getUnitSphereMesh();
Mesh getPlaneMesh();
Mesh getBoxMesh();

#endif