#include "SceneManager.hpp"

SceneManager::~SceneManager(){
	clear();

	// Delete shaders
	for(auto& pair: shaderProgramCache){
		glDeleteProgram(pair.second);
	}

	// Delete meshes
	for(auto& pair: meshCache){
		pair.second.destroy();
	}
};

void SceneManager::clear(){
	for(auto& pair: entities){
		pair.second.clear();
	}
};

Entity* SceneManager::createEntity(const std::string& shaderProgramName, const std::string& meshName){
	GLuint shaderID = shaderProgramCache[shaderProgramName];

	std::stringstream ss;

	if(meshCache[meshName].isEmpty()){
		ss << "Could not create entity with mesh: " << meshName << ".\n";
		ss << "No mesh associated with this identifier.\n";
		std::cerr << ss.str();
		Logger::error(ss.str());
		return nullptr;
	}

	entities[shaderID].emplace_back(this, idPool++);
	entities[shaderID].back().setMesh(&meshCache[meshName]);
	entities[shaderID].back().getUniforms().setProgramID(shaderID);

	ss << "New entity created:\nMesh: " << meshName << "\nShader: " << shaderProgramName << "\n";
	Logger::log(ss.str());

	return &entities[shaderID].back();
};

void SceneManager::removeEntity(Entity* entity){ // Note, entity must still be deleted externally.
	entities[entity->getUniforms().getProgramID()].remove(*entity);
};

void SceneManager::render(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for(auto& pair: entities){
		glUseProgram(pair.first);
		globalUniforms[pair.first].activate();

		for(Entity& entity: pair.second){
			entity.getUniforms().addMat4Uniform(UniformNames::MODEL_UNIFORM, entity.getModelMatrix());
			entity.getUniforms().addMat4Uniform(UniformNames::MVP_UNIFORM, camera.getVPMatrix() * entity.getModelMatrix());
			entity.getUniforms().activate();

			entity.render();
		}
	}
};

void SceneManager::update(double deltaTime){
	physicsWorld.update(timeStep, 10);

	camera.update(deltaTime);
	
	for(auto& pair: entities){
		for(Entity& entity: pair.second){
			entity.update(deltaTime);
		}
	}
};

void SceneManager::loadShader(const std::string& name, const std::string& vs, const std::string& fs){
	if(shaderProgramCache.find(name) != shaderProgramCache.end())
		return;

	std::stringstream ss;

	GLuint programID = loadGLProgram(vs, fs);
	if(programID == 0){
		ss << "Unable to load the following GL program:\n";
		ss << "Vertex shader: " << vs << "\n";
		ss << "Fragment shader: " << fs << "\n";
		std::cerr << ss.str();
		Logger::error(ss.str());
		return;
	}

	ss << "New shader program successfully created:\n";
	ss << "Vertex shader: " << vs << "\nFragment shader: " << fs << "\n";
	Logger::log(ss.str());

	shaderProgramCache[name] = programID;
	globalUniforms[programID] = Uniforms(programID);
};

void SceneManager::loadMesh(const std::string& name, ShapeMeshType shape){
	if(meshCache.find(name) != meshCache.end())
		return;

	meshCache[name] = getShapeMesh(shape);

	std::stringstream ss;
	ss << "Successfully loaded shape mesh: " << static_cast<std::underlying_type<ShapeMeshType>::type>(shape) << "\n";
	Logger::log(ss.str());
};

void SceneManager::loadMesh(const std::string& name, const std::string& filePath){
	if(meshCache.find(name) != meshCache.end())
		return;

	// TODO: Load mesh from file instead
};

const Entity* SceneManager::getEntity(int entityID) const {
	for(auto& pair: entities){
		for(const Entity& entity: pair.second){
			if(entity.getEntityID() == entityID)
				return &entity;
		}
	}

	return nullptr;
};
