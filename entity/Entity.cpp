#include "Entity.hpp"
#include "../rendering/SceneManager.hpp"

Entity::~Entity(){
	removeRigidBody();
};

void Entity::render() const{
	if(mesh != nullptr)
		mesh->render();
};

void Entity::update(double deltaTime){
	if(rigidBody != nullptr){
		modelMatrix = rigidBody->getModelMatrix();
	}
};

void Entity::setRigidBody(RigidBody* rigidBody){
	removeRigidBody();
	this->rigidBody = rigidBody;
	manager->getWorld().addRigidBody(rigidBody);
}

void Entity::removeRigidBody(){
	if(rigidBody != nullptr){
		manager->getWorld().removeRigidBody(rigidBody);
		delete rigidBody;
	}
};