#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../physics/RigidBody.hpp"
#include "../rendering/Mesh.hpp"
#include "../rendering/Uniforms.hpp"

class SceneManager;

class Entity{

	public:
		// TODO: More complex entities.
		Entity(SceneManager* manager, int entityID): 
			manager(manager),
			mesh(nullptr),
			entityID(entityID),
			rigidBody(nullptr)
		{};

		~Entity();

		void render() const;
		void update(double deltaTime);

		Mesh* getMesh() const{return mesh;}
		void setMesh(Mesh* mesh){this->mesh = mesh;};

		Uniforms& getUniforms(){return uniforms;};
		int getEntityID() const {return entityID;};

		glm::mat4 getModelMatrix() const {return modelMatrix;};
		void setPosition(glm::vec3& position){modelMatrix = glm::translate(glm::mat4(1.0f), position);};
		void setPosition(float x, float y, float z){modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));};

		RigidBody* getRigidBody() const {return rigidBody;};
		void setRigidBody(RigidBody* rigidBody);

		bool operator==(const Entity& other){return this->getEntityID() == other.getEntityID();};

	private:
		void removeRigidBody();

		SceneManager* manager;

		Mesh* mesh;
		int entityID;
		
		Uniforms uniforms; 
		
		glm::mat4 modelMatrix;

		RigidBody* rigidBody;

};

#endif
