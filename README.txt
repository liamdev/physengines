Physics Engine Test Application
================================

This repository contains the source code for an application which runs a
variety of physical simulations with basic rigid bodies, in order to evaluate 
the flexibility and accuracy of five of the most popular physics engines:

- Bullet (http://bulletphysics.org/)
- Havok (http://www.havok.com/)
- Newton Game Dynamics (http://newtondynamics.com/forum/newton.php)
- Open Dynamics Engine (http://http://www.ode.org/)
- PhysX (https://developer.nvidia.com/physx-sdk)

The application runs six simulations, using a simulation time step of 0.01
seconds. These six tests are:

1. A ball with a radius of 1m and mass of 1kg is dropped onto a 1x1x1m box,
using a restitution of 0.5. The resulting bounce height is measured.

2. A ball with a radius of 1m and mass of 1kg is dropped into thin air. Its
vertical offset from its starting point is measured at each time step.

3. Three spheres with radius of 1m are dropped onto a fixed plane, with a
vertical gap of 0.1m between them. In reality, the spheres should not stack
neatly. The inherent noise would cause them to roll off of each other. No
results are logged, but this test provides a visual check of the engine's
ability to provide realistic results.

4. Three 1x1x1m boxes are dropped onto a fixed plane, with a vertical gap of
0.8m between them. No results are logged, but this test provides a visual check
of the engine's ability to handle stacked rigid bodies.

5. A 1x1x1m box is placed on a flat plane, each with a static friction
coefficient of 0.5. The plane's angle is incremented by 0.05 radians every
second, until the box begins to slide. The angle required to overcome the
static friction is logged.

6. Four balls are placed in a 2x2 grid above an inverted, open-bottomed pyramid.
The balls drop into the pyramid, and settle. The total interpenetration between
the spheres and the planes which make up the pyramid walls is recorded.

Build instructions:
-------------------------

To build the included source code, you will need the following libraries.
Earlier versions of these libraries may be compatible, but no guarantees can be
made:

- GLFW 3.0.4 (http://www.glfw.org/)
- GLEW 1.10.0 (http://glew.sourceforge.net/)
- GLM 0.9.5.2 (http://glm.g-truc.net/)

The application was written against the following physics engines. You do not
need all of these to compile the application. You only require those engines
which you wish to build against. Again, no guarantees of intended behaviour
are provided for earlier library versions:

- Bullet 2.82 (http://bulletphysics.org/)
- Havok 2013.1-6 (http://www.havok.com/)
- Newton Game Dynamics 3.11 (http://newtondynamics.com/forum/newton.php)
- Open Dynamics Engine 0.13 (http://http://www.ode.org/)
- PhysX 3.3.0 (https://developer.nvidia.com/physx-sdk)

The source code has been tested against Visual C++ 2012, and gcc/g++ version
4.8.2. To build against a specific engine, you must provide a preprocessor
definition. By default, the application will attempt to build against Bullet.
To change the build target, provide one of the following definitions:

-DUSE_HAVOK_BACKEND
-DUSE_NEWTON_BACKEND
-DUSE_ODE_BACKEND
-DUSE_PHYSX_BACKEND

Makefiles have been provided in the repository to speed up the build process
with g++. These makefiles assume that Bullet's includes are found in
/usr/include/bullet, and that Newton's are found in /usr/include/newton.
The makefile only provides build options for Bullet, ODE, and Newton, since
Havok and PhysX do not provide Linux versions. To build against each of the
engines, use one of the following commands:

make			- builds the default Bullet application.
make debug		- builds the debug version of the Bullet application.
make ode		- builds the default ODE application.
make oded		- builds the debug version of the ODE application.
make newton		- builds the default Newton application.
make newtond		- builds the debug version of the Newton application.

The result of the makefile build process is an application with the name
runbullet, runnewton, or runode. The applications use the C++11 standard.

Running the application:
-------------------------

Compile the library as detailed in the above build instructions. The resulting
executable can be run to simulate test 1 with the engine it was built against.
In order to run another test, you must provide program arguments in the
following form:

executablename [-n testNumber]

Where testNumber is an integer from 1-6, corresponding to the tests described
above.

Controls:
-------------------------

WSAD/Arrow Keys: Move camera
Mouse: Change camera direction
Shift: Activate camera speed boost
Escape: Exit the application
