#include "GLFWTools.hpp"

GLFWwindow* initGLFWWithWindow(int width, int height, const char* title, bool resizable){
	glfwSetErrorCallback(handleGLFWError);

	if(!glfwInit()){
		return nullptr;
	}

	glfwWindowHint(GLFW_RESIZABLE, resizable);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	GLFWwindow* window = glfwCreateWindow(width, height, title, nullptr, nullptr);
	if(!window){
		return nullptr;
	}

	glfwMakeContextCurrent(window);
	
	return window;
}

void setGLFWInputHandler(GLFWwindow* window, InputHandler& handler){
	Input::setHandler(handler);
	glfwSetKeyCallback(window, Input::inputCallback);
	glfwSetCursorPosCallback(window, Input::mousePosCallback);
}

void handleGLFWError(int errorCode, const char* errDescription){
	std::stringstream ss;
	ss << "Error " << errorCode << ": " << errDescription << "\n";

	std::cerr << ss.str();
	Logger::error(ss.str());
}

void closeGLFWWithWindow(GLFWwindow* window){
	glfwDestroyWindow(window);
	glfwTerminate();
};