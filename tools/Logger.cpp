#include "Logger.hpp"

std::string Logger::logFilePath = "";
LogLevel Logger::logLevel = LogLevel::WARNING;

void Logger::startLogger(const std::string& outputDirectory, LogLevel level){
	logLevel = level;

	time_t t = std::time(nullptr);
	tm* localTime = std::localtime(&t);
	char datetime[16], date[9], time[7];
	strftime(datetime, 16, "%Y%m%d-%H%M%S", localTime);
	strftime(date, 9, "%d/%m/%Y", localTime);
	strftime(time, 7, "%H:%M:%S", localTime);

	std::stringstream ss;
	ss << datetime << ".log";
	logFilePath = outputDirectory + ss.str();

	ss.str(std::string());
	ss.clear();

	ss << "Logger successfully started on ";
	ss << date << " at " << time << "\n";
	log(ss.str());
};

void Logger::log(const std::string& msg, LogLevel level){
	if(level < logLevel){
		return;
	}

	if(logFilePath.empty()){
		std::cerr << "Warning: log attempted, but the logger has not been initialised.\n";
		return;
	}

	std::ofstream logStream;
	logStream.open(logFilePath, std::ios::app);
	if(logStream.is_open()){
		logStream << msg << "\n";
	}
	logStream.close();
};

void Logger::logIf(bool condExpr, const std::string& msg, LogLevel level){
	if(condExpr)
		log(msg, level);
};

void Logger::warning(const std::string& msg){
	std::stringstream ss;
	ss << "[WARNING]: " << msg << "\n";
	log(ss.str(), LogLevel::WARNING);
};

void Logger::error(const std::string& msg){
	std::stringstream ss;
	ss << "===========\n";
	ss << "[ERROR]\n";
	ss << "===========\n";
	ss << msg;
	log(ss.str(), LogLevel::ERROR);
};
