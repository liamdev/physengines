#ifndef GL_TOOLS_HPP
#define GL_TOOLS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <GL/glew.h>

GLuint loadGLProgram(const std::string& vsFilepath, const std::string& fsFilepath);

#endif