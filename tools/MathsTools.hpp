#ifndef MATHS_TOOLS_HPP
#define MATHS_TOOLS_HPP

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../app/Config.hpp"

float getHorizontalAngleRads(glm::vec3 vec);

glm::mat4 getRotationFromTo(glm::vec3 src, glm::vec3 dest);

float getVecMagnitudeSquared(const glm::vec3& vec);

glm::quat getQuaternionFromTo(glm::vec3 src, glm::vec3 dest);

#endif