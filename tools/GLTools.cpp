#include "GLTools.hpp"

bool loadShader(GLuint shaderId, const std::string& filepath){
	std::ifstream file(filepath.c_str());

	if(!file){
		std::cerr << "Shader file not found: " << filepath << "\n\n";
		return false;
	}

	std::string shaderCode;
	std::string line;
	while(std::getline(file, line))
		shaderCode += line + "\n";

	file.close();

	const char* shaderCodePtr = shaderCode.c_str();
	glShaderSource(shaderId, 1, &shaderCodePtr, NULL);
	glCompileShader(shaderId);

	GLint compileStatus;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compileStatus);

	if(compileStatus == GL_FALSE){
		GLint infoLogLength;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);

		std::vector<char> compilerOutput(infoLogLength);
		glGetShaderInfoLog(shaderId, infoLogLength, NULL, &compilerOutput[0]);

		std::cerr << "Shader compilation was unsuccessful. Compiler output:\n";
		std::cerr << &compilerOutput[0] << "\n";

		return false;
	}

	return true;
};

GLuint loadGLProgram(const std::string& vsFilepath, const std::string& fsFilepath){

	GLuint vShaderId = glCreateShader(GL_VERTEX_SHADER);
	GLuint fShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	if(!loadShader(vShaderId, vsFilepath) || !loadShader(fShaderId, fsFilepath)){
		glDeleteShader(vShaderId);
		glDeleteShader(fShaderId);
		return 0;
	}

	GLuint programId = glCreateProgram();
	glAttachShader(programId, vShaderId);
	glAttachShader(programId, fShaderId);
	glLinkProgram(programId);

	GLint linkStatus;
	glGetProgramiv(programId, GL_LINK_STATUS, &linkStatus);

	if(linkStatus == GL_FALSE){
		GLint infoLogLength;
		glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLogLength);

		std::vector<char> compilerOutput(infoLogLength);
		glGetProgramInfoLog(programId, infoLogLength, NULL, &compilerOutput[0]);

		std::cerr << "Shader program link was unsuccessful. Linker output:\n";
		std::cerr << &compilerOutput[0] << "\n";

		return 0;
	}

	glDetachShader(programId, vShaderId);
	glDetachShader(programId, fShaderId);

	glDeleteShader(vShaderId);
	glDeleteShader(fShaderId);

	return programId;
};
