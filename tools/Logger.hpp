#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

enum class LogLevel : char {INFO = 0, DEBUG = 1, WARNING = 2, ERROR = 3};

class Logger{
	
	public:
		static void startLogger(const std::string& outputDirectory, LogLevel logLevel = LogLevel::WARNING);

		static void log(const std::string& msg, LogLevel level = LogLevel::INFO);
		static void logIf(bool evalExpr, const std::string& msg, LogLevel level = LogLevel::INFO);

		static void warning(const std::string& msg);
		static void error(const std::string& msg);

	private:
		static std::string logFilePath;
		static LogLevel logLevel;
};

#endif
