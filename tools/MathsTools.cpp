#include "MathsTools.hpp"

float getHorizontalAngleRads(glm::vec3 vec){
	return atan(vec.x / vec.z);
};

glm::mat4 getRotationFromTo(glm::vec3 src, glm::vec3 dest){
	glm::vec3 orig = glm::normalize(src);
	glm::vec3 forward = glm::normalize(dest);

	float d = glm::dot(orig, forward);
	if(d > 0.9999f){
		return glm::mat4(1.0f);
	} else if(d < -0.9999f){
		return glm::mat4(1.0f); // TODO: Fix for opposite colinear vectors?
	}

	glm::vec3 right = glm::normalize(glm::cross(orig, forward));
	glm::vec3 up = glm::normalize(glm::cross(forward, right));

	glm::mat4 rot(1.0f);
	for(int i = 0; i < 3; ++i)
		rot[0][i] = right[i];
	for(int i = 0; i < 3; ++i)
		rot[1][i] = forward[i];
	for(int i = 0; i < 3; ++i)
		rot[2][i] = up[i];

	return rot;
};

float getVecMagnitudeSquared(const glm::vec3& vec){
	return vec.x * vec.x + vec.y * vec.y + vec.z * vec.z;
};

glm::quat getQuaternionFromTo(glm::vec3 src, glm::vec3 dest){
	glm::vec3 v1 = glm::normalize(src);
	glm::vec3 v2 = glm::normalize(dest);

	if(glm::dot(v1, v2) > 0.9999f){
		return glm::quat(1, 0, 0, 0);
	}

	glm::vec3 axis = glm::normalize(glm::cross(v1, v2));
	float angle = std::acos(glm::dot(v1, v2)) * Maths::RADS_TO_DEG;

	return glm::angleAxis(angle, axis);
};
