#ifndef	GLFW_TOOLS_HPP
#define GLFW_TOOLS_HPP

#include <iostream>
#include <sstream>

#include <GLFW/glfw3.h>

#include "../input/InputHandler.hpp"
#include "Logger.hpp"

GLFWwindow* initGLFWWithWindow(int width, int height, const char* title, bool resizable = true);

void setGLFWInputHandler(GLFWwindow* window, InputHandler& handler);

void handleGLFWError(int errorCode, const char* errDescription);

void closeGLFWWithWindow(GLFWwindow* window);

#endif