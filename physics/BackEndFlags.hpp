#ifndef BACKEND_FLAGS_HPP
#define BACKEND_FLAGS_HPP

#include <string>

#ifdef USE_HAVOK_BACKEND // Havok Engine
#include <Common/Base/hkBase.h>
#include <Physics2012/Dynamics/hkpDynamics.h>
// Exclude unnecessary Havok modules
#undef HK_FEATURE_PRODUCT_AI
#undef HK_FEATURE_PRODUCT_ANIMATION
#undef HK_FEATURE_PRODUCT_CLOTH
#undef HK_FEATURE_PRODUCT_DESTRUCTION_2012
#undef HK_FEATURE_PRODUCT_DESTRUCTION
#undef HK_FEATURE_PRODUCT_BEHAVIOR
#undef HK_FEATURE_PRODUCT_PHYSICS_2012
#undef HK_FEATURE_PRODUCT_PHYSICS
#define HK_EXCLUDE_FEATURE_SerializeDeprecatedPre700
#define HK_EXCLUDE_FEATURE_RegisterVersionPatches
#define HK_EXCLUDE_FEATURE_RegisterReflectedClasses
#define HK_EXCLUDE_FEATURE_MemoryTracker
const std::string PHYS_ENGINE_NAME = "havok";

#elif defined USE_PHYSX_BACKEND // PhysX Engine
#include <PxPhysicsAPI.h>
const std::string PHYS_ENGINE_NAME = "physx";

#elif defined USE_ODE_BACKEND // Open Dynamics Engine
#include <ode/ode.h>
const std::string PHYS_ENGINE_NAME = "ode";

#elif defined USE_NEWTON_BACKEND // Newton Game Dynamics Engine
#include <newton/Newton.h>
const std::string PHYS_ENGINE_NAME = "newton";

#else // Bullet Engine
#ifndef USE_BULLET_BACKEND
#define USE_BULLET_BACKEND
#endif
#include <btBulletDynamicsCommon.h>
const std::string PHYS_ENGINE_NAME = "bullet";

#endif // Physics engine cases

#endif // BACKEND_FLAGS_HPP
