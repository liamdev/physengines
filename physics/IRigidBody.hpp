#ifndef IRIGID_BODY_HPP
#define IRIGID_BODY_HPP

#include <glm/glm.hpp>

class IRigidBody{
	public:
		virtual ~IRigidBody(){};

		virtual glm::mat4 getModelMatrix() const = 0;

		virtual glm::vec3 getVelocity() const = 0;

		virtual void setFriction(float staticFriction, float dynamicFriction) = 0;

		virtual void applyRotation(const glm::vec3& axis, float angleRads) = 0;
};

#endif
