#include "NewtonRigidBody.hpp"

int RigidBody::idPool = 0;

RigidBody::RigidBody(const PlaneRigidBody& plane, World& world) : world(world), id(idPool++){
	glm::vec3 pos(0, plane.height, 0);
	glm::quat q = getQuaternionFromTo(glm::vec3(0, 1, 0), glm::vec3(plane.normalX, plane.normalY, plane.normalZ));
	glm::mat4 transform = glm::translate(glm::mat4(1.0f), pos) * glm::mat4_cast(q);

	collider = std::unique_ptr<NewtonCollision, NewtonColliderDestroyer>(
					NewtonCreateBox(world.getWorld(),
					10000, 0.0001f, 10000, id, nullptr)
				);

	body = std::unique_ptr<NewtonBody, NewtonBodyDestroyer>(
					NewtonCreateDynamicBody(world.getWorld(),
					collider.get(),
					&transform[0][0])
				);

	NewtonBodySetMassMatrix(body.get(), plane.mass, 1.0f, 1.0f, 1.0f);
};

RigidBody::RigidBody(const BoxRigidBody& box, World& world) : world(world), id(idPool++){
	glm::vec3 pos(box.xPos, box.yPos, box.zPos);
	glm::mat4 transform = glm::translate(glm::mat4(1.0f), pos);

	collider = std::unique_ptr<NewtonCollision, NewtonColliderDestroyer>(
					NewtonCreateBox(world.getWorld(),
					box.width, box.height, box.depth,
					id, nullptr)
				);

	body = std::unique_ptr<NewtonBody, NewtonBodyDestroyer>(
					NewtonCreateDynamicBody(world.getWorld(),
					collider.get(),
					&transform[0][0])
				);

	if(!box.stationary){
		NewtonBodySetMassMatrix(body.get(), box.mass, 1.0f, 1.0f, 1.0f);
	}
};

RigidBody::RigidBody(const SphereRigidBody& sphere, World& world) : world(world), id(idPool++){
	glm::vec3 pos(sphere.xPos, sphere.yPos, sphere.zPos);
	glm::mat4 transform = glm::translate(glm::mat4(1.0f), pos);

	collider = std::unique_ptr<NewtonCollision, NewtonColliderDestroyer>(
					NewtonCreateSphere(world.getWorld(),
					sphere.radius,
					id, nullptr)
				);

	body = std::unique_ptr<NewtonBody, NewtonBodyDestroyer>(
					NewtonCreateDynamicBody(world.getWorld(),
					collider.get(),
					&transform[0][0])
				);

	NewtonBodySetMassMatrix(body.get(), sphere.mass, 1.0f, 1.0f, 1.0f);
};

RigidBody::~RigidBody(){
};

glm::mat4 RigidBody::getModelMatrix() const{
	glm::mat4 transform(1.0f);
	NewtonBodyGetMatrix(body.get(), &transform[0][0]);
	return transform;
};

glm::vec3 RigidBody::getVelocity() const{
	float vel[3];
	NewtonBodyGetVelocity(body.get(), vel);
	return glm::vec3(vel[0], vel[1], vel[2]);
};

void RigidBody::setFriction(float staticFriction, float dynamicFriction){
	int materialGroup = NewtonBodyGetMaterialGroupID(body.get());
	NewtonMaterialSetDefaultFriction(world.getWorld(), materialGroup, materialGroup, staticFriction, dynamicFriction);
};

int bodyWakeCallback(const NewtonBody* body, void* userData){
	NewtonBodySetFreezeState(body, true);
	NewtonBodySetSleepState(body, true);
	return 0;
};

void RigidBody::applyRotation(const glm::vec3& axis, float angleRads){
	glm::mat4 mat;
	NewtonBodyGetMatrix(body.get(), &mat[0][0]);
	// Add an impulse to get it moving. Not strictly part of the rotation,
	// but in this codebase this function is only used in the rotating plane
	// test, in which case the box must be displaced to initiate motion.
	float vel[3] = {0, 0.2f, 0};
	float point[3] = {0, 0, 0};
	NewtonBodyAddImpulse(body.get(), vel, point); 
	mat = glm::rotate(mat, angleRads * Maths::RADS_TO_DEG, axis);
	NewtonBodySetMatrix(body.get(), &mat[0][0]);
};