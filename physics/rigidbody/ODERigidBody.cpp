#include "ODERigidBody.hpp"

RigidBody::RigidBody(const PlaneRigidBody& plane, World& world) :
	world(world),
	modelMatrix(glm::mat4(1.0f)),
	bodyID(0),
	geomID(0)
{
	// Assumes that the plane is at (0, height, 0) in world coordinates.
	//geomID = dCreatePlane(world.getSpace(), plane.normalX, plane.normalY, plane.normalZ, plane.normalY * plane.height);
	geomID = dCreateBox(world.getSpace(), 100, 0.01f, 100);

	glm::vec3 normal(plane.normalX, plane.normalY, plane.normalZ);
	glm::quat q = getQuaternionFromTo(glm::vec3(0, 1, 0), normal);

	dReal q2[4];
	q2[0] = q[1]; q2[1] = q[2];	q2[2] = q[3]; q2[3] = q[0];

	dGeomSetQuaternion(geomID, q2);
	modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0, plane.height, 0)) * glm::mat4_cast(q);
	friction[0] = 1;
	friction[1] = 1;
};

RigidBody::RigidBody(const BoxRigidBody& box, World& world) :
	world(world),
	modelMatrix(glm::mat4(1.0f)),
	bodyID(0),
	geomID(0)
{
	geomID = dCreateBox(world.getSpace(), box.width, box.height, box.depth);

	if(box.stationary){
		dGeomSetPosition(geomID, box.xPos, box.yPos, box.zPos);
		modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(box.xPos, box.yPos, box.zPos));
		return;
	}

	bodyID = dBodyCreate(world.getWorld());
	geomID = dCreateBox(world.getSpace(), box.width, box.height, box.depth);
	dMass mass;
	dMassSetBox(&mass, box.mass, box.width, box.height, box.depth);
	dBodySetMass(bodyID, &mass);
	dGeomSetBody(geomID, bodyID);
	dBodySetPosition(bodyID, box.xPos, box.yPos, box.zPos);
	modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(box.xPos, box.yPos, box.zPos));
	friction[0] = 1;
	friction[1] = 1;
};

RigidBody::RigidBody(const SphereRigidBody& sphere, World& world) :
	world(world),
	modelMatrix(glm::mat4(1.0f)),
	bodyID(0),
	geomID(0)
{
	bodyID = dBodyCreate(world.getWorld());
	geomID = dCreateSphere(world.getSpace(), sphere.radius);
	dMass mass;
	dMassSetSphere(&mass, sphere.mass, sphere.radius);
	dBodySetMass(bodyID, &mass);
	dGeomSetBody(geomID, bodyID);
	dBodySetPosition(bodyID, sphere.xPos, sphere.yPos, sphere.zPos);
	modelMatrix = getModelMatrix();
	friction[0] = 1;
	friction[1] = 1;
};

RigidBody::~RigidBody(){
	if(geomID > 0){
		dGeomDestroy(geomID);
	}
	if(bodyID > 0){
		dBodyDestroy(bodyID);
	}
};

glm::mat4 RigidBody::getModelMatrix() const{
	if(bodyID == 0) // Plane, no rigid body object associated, fixed position;
		return modelMatrix;

	const dReal* pos = dBodyGetPosition(bodyID);
	const dReal* quat = dBodyGetQuaternion(bodyID);

	glm::mat4 translation = glm::translate(glm::mat4(1.0f), glm::vec3(pos[0], pos[1], pos[2]));
	glm::mat4 rotation = glm::mat4_cast(glm::quat(quat[0], quat[1], quat[2], quat[3]));

	return translation * rotation;
};

glm::vec3 RigidBody::getVelocity() const{
	if(bodyID == 0){
		return glm::vec3(0, 0, 0);
	}

	const dReal* vel = dBodyGetLinearVel(bodyID);
	return glm::vec3(vel[0], vel[1], vel[2]);
};

void RigidBody::setFriction(float staticFriction, float dynamicFriction){
	friction[0] = staticFriction;
	friction[1] = dynamicFriction;
	dGeomSetData(geomID, friction);
};

void RigidBody::applyRotation(const glm::vec3& axis, float angleRads){
	if(bodyID == 0){
		dReal q1[4];
		dGeomGetQuaternion(geomID, q1);

		glm::quat q2(q1[0], q1[1], q1[2], q1[3]);
		float angleDeg = angleRads * Maths::RADS_TO_DEG;
		q2 = glm::rotate(q2, -angleDeg, axis);
		q1[0] = q2.w;
		q1[1] = q2.x;
		q1[2] = q2.y;
		q1[3] = q2.z;

		dGeomSetQuaternion(geomID, q1);
		modelMatrix = glm::rotate(modelMatrix, angleDeg, axis);
		return;
	}

	std::stringstream ss;
	ss << "Can only apply direct rotation to static bodies.\n";
	ss << "No rotation has been applied.\n";
	Logger::warning(ss.str());
};