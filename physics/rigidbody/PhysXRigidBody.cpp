#include "PhysXRigidBody.hpp"

RigidBody::RigidBody(const PlaneRigidBody& plane, World& world) :
	world(world),
	staticActor(nullptr),
	dynamicActor(nullptr),
	material(nullptr)
{
	glm::vec3 planeNormal(plane.normalX, plane.normalY, plane.normalZ);
	planeNormal = glm::normalize(planeNormal);
	physx::PxPlane planeDesc(planeNormal.x, planeNormal.y, planeNormal.z, plane.height);

	glm::quat q = getQuaternionFromTo(glm::vec3(0, 1, 0), planeNormal);

	material = world.getWorld()->createMaterial(1.0f, 1.0f, plane.restitution);

	staticTransform = glm::translate(glm::mat4(1.0f), glm::vec3(0, plane.height, 0)) * glm::mat4_cast(q);
	staticActor = physx::PxCreatePlane(*(world.getWorld()), planeDesc, *material);
};

RigidBody::RigidBody(const BoxRigidBody& box, World& world) :
	world(world),
	staticActor(nullptr),
	dynamicActor(nullptr),
	material(nullptr)
{
	physx::PxTransform transform(physx::PxVec3(box.xPos, box.yPos, box.zPos));
	physx::PxBoxGeometry geometry(box.width / 2.0f, box.height / 2.0f, box.depth / 2.0f);

	material = world.getWorld()->createMaterial(0.5f, 0.5f, box.restitution);

	if(box.stationary){
		staticActor = physx::PxCreateStatic(*world.getWorld(), transform, geometry, *material);
		staticTransform = glm::translate(glm::mat4(1.0f), glm::vec3(box.xPos, box.yPos, box.zPos));
	} else {
		dynamicActor = physx::PxCreateDynamic(*world.getWorld(), transform, geometry, *material, box.mass);
		dynamicActor->setMass(box.mass);
	}
};

RigidBody::RigidBody(const SphereRigidBody& sphere, World& world) :
	world(world),
	staticActor(nullptr),
	dynamicActor(nullptr),
	material(nullptr)
{
	physx::PxTransform transform(physx::PxVec3(sphere.xPos, sphere.yPos, sphere.zPos));
	physx::PxSphereGeometry geometry(sphere.radius);
	
	material = world.getWorld()->createMaterial(0.5f, 0.5f, sphere.restitution);

	dynamicActor = physx::PxCreateDynamic(*world.getWorld(), transform, geometry, *material, sphere.mass);
	dynamicActor->setMass(sphere.mass);
};

RigidBody::~RigidBody(){
	// Rigid body deletion handled automatically when the physics world is removed.
	// Would be an issue if removing rigid bodies mid-simulation for memory purposes,
	// but this is out of scope.
};

glm::mat4 RigidBody::getModelMatrix() const{
	if(this->isStaticActor()){
		return staticTransform;
	} 

	physx::PxTransform tf = dynamicActor->getGlobalPose();

	glm::quat q(tf.q.w, tf.q.x, tf.q.y, tf.q.z);
	glm::mat4 orientation = glm::mat4_cast(q);
	glm::mat4 translation = glm::translate(glm::mat4(1.0f), glm::vec3(tf.p.x, tf.p.y, tf.p.z));
	return translation * orientation;
};

glm::vec3 RigidBody::getVelocity() const{
	if(this->isStaticActor()){
		std::stringstream ss;
		ss << "Cannot retrieve velocity for a static rigid body.\n";
		std::cout << ss.str();
		Logger::warning(ss.str());
		return glm::vec3(0, 0, 0);
	} else {
		physx::PxVec3 vel = dynamicActor->getLinearVelocity();
		return glm::vec3(vel.x, vel.y, vel.z);
	}
};

void RigidBody::setFriction(float staticFriction, float dynamicFriction){
	material->setStaticFriction(staticFriction);
	material->setDynamicFriction(dynamicFriction);
};

void RigidBody::applyRotation(const glm::vec3& axis, float angleRads){
	physx::PxTransform tf;
	if(this->isStaticActor()){
		tf = staticActor->getGlobalPose();
	} else {
		tf = dynamicActor->getGlobalPose();
	}
	
	glm::quat q(tf.q.w, tf.q.x, tf.q.y, tf.q.z);
	float angleDeg = angleRads * Maths::RADS_TO_DEG;
	q = glm::rotate(q, angleDeg, axis);

	physx::PxTransform tf2(physx::PxVec3(tf.p.x, tf.p.y, tf.p.z),
							physx::PxQuat(q.x, q.y, q.z, q.w));

	if(this->isStaticActor()){
		staticActor->setGlobalPose(tf2);
		staticTransform = glm::rotate(staticTransform, angleDeg, axis);
	} else {
		dynamicActor->setGlobalPose(tf2);
		dynamicActor->wakeUp();
	}
};