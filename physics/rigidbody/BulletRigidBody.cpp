#include "BulletRigidBody.hpp"

RigidBody::RigidBody(const PlaneRigidBody& plane, World& world) : world(world), stationary(true){
	glm::vec3 normal = glm::normalize(glm::vec3(plane.normalX, plane.normalY, plane.normalZ));
	//btVector3 planeNormal(normal.x, normal.y, normal.z);
	btVector3 planeNormal(0, 1, 0);
	collisionShape = std::unique_ptr<btStaticPlaneShape>(new btStaticPlaneShape(planeNormal, plane.height));

	btVector3 inertia(0, 0, 0);
	collisionShape->calculateLocalInertia(plane.mass, inertia);

	glm::quat q = getQuaternionFromTo(glm::vec3(0, 1, 0), normal);

	btTransform planeTf(btQuaternion(q.x, q.y, q.z, q.w), btVector3(0, plane.height, 0));
	btDefaultMotionState* motionState = new btDefaultMotionState(planeTf);
	btRigidBody::btRigidBodyConstructionInfo constructionInfo(plane.mass, motionState, collisionShape.get(), inertia);

	rigidBody = std::unique_ptr<btRigidBody>(new btRigidBody(constructionInfo));
	rigidBody->setRestitution(plane.restitution);
};

RigidBody::RigidBody(const BoxRigidBody& box, World& world) : world(world), stationary(false){
	btVector3 halfExtents(box.width / 2.0f, box.height / 2.0f, box.depth / 2.0f);
	collisionShape = std::unique_ptr<btBoxShape>(new btBoxShape(halfExtents));

	float bMass = box.stationary ? 0 : box.mass;

	btVector3 inertia(0, 0, 0);
	collisionShape->calculateLocalInertia(bMass, inertia);

	btTransform boxTf(btQuaternion(0, 0, 0, 1), btVector3(box.xPos, box.yPos, box.zPos));
	btDefaultMotionState* motionState = new btDefaultMotionState(boxTf);
	btRigidBody::btRigidBodyConstructionInfo constructionInfo(bMass, motionState, collisionShape.get(), inertia);
	
	rigidBody = std::unique_ptr<btRigidBody>(new btRigidBody(constructionInfo));
	rigidBody->setRestitution(box.restitution);
};

RigidBody::RigidBody(const SphereRigidBody& sphere, World& world) : world(world), stationary(false){
	collisionShape = std::unique_ptr<btSphereShape>(new btSphereShape(sphere.radius));

	btVector3 inertia(0, 0, 0);
	collisionShape->calculateLocalInertia(sphere.mass, inertia);

	btVector3 spherePos(sphere.xPos, sphere.yPos, sphere.zPos);
	btTransform sphereTf(btQuaternion(0, 0, 0, 1), spherePos);
	btDefaultMotionState* motionState = new btDefaultMotionState(sphereTf);
	btRigidBody::btRigidBodyConstructionInfo constructionInfo(sphere.mass, motionState, collisionShape.get(), inertia);

	rigidBody = std::unique_ptr<btRigidBody>(new btRigidBody(constructionInfo));
	rigidBody->setRestitution(sphere.restitution);
};

RigidBody::~RigidBody(){
	// Rigid body and collision shape are automatically deleted by the semantics of unique_ptr.
	// Still need to delete the motion state.
	delete rigidBody->getMotionState();
};

glm::mat4 RigidBody::getModelMatrix() const{
	if(stationary){
		btTransform& transform = rigidBody->getWorldTransform();
		glm::mat4 tf(1.0f);
		transform.getOpenGLMatrix(&tf[0][0]);
		return tf;
	} else {
		btTransform transform;
		rigidBody->getMotionState()->getWorldTransform(transform);
		glm::mat4 tf(1.0f);
		transform.getOpenGLMatrix(&tf[0][0]);
		return tf;
	}
};

glm::vec3 RigidBody::getVelocity() const{
	btVector3 vel = rigidBody->getInterpolationLinearVelocity();
	return glm::vec3(vel.x(), vel.y(), vel.z());
};

void RigidBody::setFriction(float staticFriction, float dynamicFriction){
	rigidBody->setFriction(dynamicFriction);
}

void RigidBody::applyRotation(const glm::vec3& axis, float angleRads){
	btMatrix3x3 rot = rigidBody->getWorldTransform().getBasis();
	rot *= btMatrix3x3(btQuaternion(btVector3(axis.x, axis.y, axis.z), angleRads));
	rigidBody->getWorldTransform().setBasis(rot);
	rigidBody->activate(true);
};