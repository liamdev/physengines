#include "HavokRigidBody.hpp"

RigidBody::RigidBody(const PlaneRigidBody& plane, World& world) :
	world(world)
{
	glm::vec3 planeNormal = glm::normalize(glm::vec3(plane.normalX, plane.normalY, plane.normalZ));
	hkVector4 normalDir(0, 1, 0);
	hkVector4 centre(0, 0, 0);
	hkVector4 halfExtents(50, 0, 50);
	hkpPlaneShape* planeShape = new hkpPlaneShape(normalDir, centre, halfExtents);

	hkpRigidBodyCinfo bodyInfo;
	bodyInfo.m_shape = planeShape;
	bodyInfo.m_position.set(0, plane.height, 0);
	bodyInfo.m_motionType = hkpMotion::MOTION_FIXED;
	bodyInfo.m_restitution = plane.restitution;

	body = new hkpRigidBody(bodyInfo);

	glm::quat q = getQuaternionFromTo(glm::vec3(0, 1, 0), planeNormal);

	planeShape->removeReference();
	body->setRotation(hkQuaternion(q.x, q.y, q.z, q.w));
};

RigidBody::RigidBody(const BoxRigidBody& box, World& world) :
	world(world)
{
	hkVector4 dimensions = hkVector4(box.width / 2.0f, box.height / 2.0f, box.depth / 2.0f);
	hkpConvexShape* boxShape = new hkpBoxShape(dimensions);

	hkpRigidBodyCinfo bodyInfo;
	bodyInfo.m_shape = boxShape;
	bodyInfo.m_position.set(box.xPos, box.yPos, box.zPos);
	bodyInfo.m_motionType = box.stationary ?
								hkpMotion::MOTION_FIXED :
								hkpMotion::MOTION_BOX_INERTIA;
	bodyInfo.m_restitution = box.restitution;

	body = new hkpRigidBody(bodyInfo);

	boxShape->removeReference();
};

RigidBody::RigidBody(const SphereRigidBody& sphere, World& world) :
	world(world)
{
	hkpConvexShape* sphereShape = new hkpSphereShape(sphere.radius);

	hkpRigidBodyCinfo bodyInfo;
	bodyInfo.m_shape = sphereShape;
	bodyInfo.m_position.set(sphere.xPos, sphere.yPos, sphere.zPos);
	bodyInfo.m_motionType = hkpMotion::MOTION_SPHERE_INERTIA;
	bodyInfo.m_mass = sphere.mass;
	bodyInfo.m_restitution = sphere.restitution;

	body = new hkpRigidBody(bodyInfo);

	sphereShape->removeReference();
};

RigidBody::~RigidBody(){
	body->removeReference();
};

glm::mat4 RigidBody::getModelMatrix() const{
	const hkTransform& hkTf = body->getTransform();

	hkDouble64 mat[16];
	hkTf.get4x4ColumnMajor(mat);

	glm::mat4 tf(1.0f);
	for(int i = 0; i < 16; ++i)
		tf[i / 4][i % 4] = static_cast<float>(mat[i]);

	return tf;
};

glm::vec3 RigidBody::getVelocity() const{
	const hkVector4& vel = body->getLinearVelocity();
	return glm::vec3(vel.getComponent(0), vel.getComponent(1), vel.getComponent(2));
};

void RigidBody::setFriction(float staticFriction, float dynamicFriction){
	body->setFriction(dynamicFriction);
}

void RigidBody::applyRotation(const glm::vec3& axis, float angleRads){
	const hkTransform& hkTf = body->getTransform();

	hkDouble64 mat[16];
	hkTf.get4x4ColumnMajor(mat);

	glm::mat4 tf(1.0f);
	for(int i = 0; i < 16; ++i)
		tf[i / 4][i % 4] = static_cast<float>(mat[i]);

	tf = glm::rotate(tf, angleRads * Maths::RADS_TO_DEG, axis);

	for(int i = 0; i < 16; ++i)
		mat[i] = tf[i / 4][i % 4];

	hkTransform hkTfRotated;
	hkTfRotated.set4x4ColumnMajor(mat);

	body->setTransform(hkTfRotated);
};