#ifndef PHYSX_RIGID_BODY_HPP
#define PHYSX_RIGID_BODY_HPP

#include <iostream>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../BackEndFlags.hpp"
#include "../IRigidBody.hpp"
#include "../RigidBody.hpp"
#include "../World.hpp"
#include "../../tools/Logger.hpp"
#include "../../tools/MathsTools.hpp"

class World;
class SphereRigidBody;
class BoxRigidBody;
class PlaneRigidBody;

class RigidBody : public IRigidBody{

	public:
		RigidBody(const SphereRigidBody& sphere, World& world);
		RigidBody(const BoxRigidBody& box, World& world);
		RigidBody(const PlaneRigidBody& plane, World& world);

		~RigidBody();

		glm::mat4 getModelMatrix() const;

		bool isStaticActor() const {return dynamicActor == nullptr;};

		glm::vec3 getVelocity() const;

		void setFriction(float staticFriction, float dynamicFriction);

		void applyRotation(const glm::vec3& axis, float angleRads);

		physx::PxRigidStatic* staticActor;
		physx::PxRigidDynamic* dynamicActor;

	private:
		physx::PxMaterial* material;
		World& world;
		glm::mat4 staticTransform;

};

#endif
