#ifndef HAVOK_RIGID_BODY_HPP
#define HAVOK_RIGID_BODY_HPP

#include <iostream>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../BackEndFlags.hpp"
#include "../IRigidBody.hpp"
#include "../RigidBody.hpp"
#include "../World.hpp"
#include "../../tools/MathsTools.hpp"

#include <Physics2012/Collide/Shape/Convex/Box/hkpBoxShape.h>
#include <Physics2012/Collide/Shape/Convex/Sphere/hkpSphereShape.h>
#include <Physics2012/Collide/Shape/HeightField/Plane/hkpPlaneShape.h>
#include <Physics2012/Dynamics/Entity/hkpRigidBody.h>

class World;
class SphereRigidBody;
class BoxRigidBody;
class PlaneRigidBody;

class RigidBody : public IRigidBody{

	public:
		RigidBody(const SphereRigidBody& sphere, World& world);
		RigidBody(const BoxRigidBody& box, World& world);
		RigidBody(const PlaneRigidBody& plane, World& world);

		~RigidBody();

		glm::mat4 getModelMatrix() const;

		glm::vec3 getVelocity() const;

		void RigidBody::setFriction(float staticFriction, float dynamicFriction);

		void RigidBody::applyRotation(const glm::vec3& axis, float angleRads);

		World& world;

		hkpRigidBody* body;
};

#endif
