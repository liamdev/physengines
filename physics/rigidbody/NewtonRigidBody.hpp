#ifndef NEWTON_RIGID_BODY_HPP
#define NEWTON_RIGID_BODY_HPP

#include <iostream>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../BackEndFlags.hpp"
#include "../IRigidBody.hpp"
#include "../World.hpp"
#include "../../tools/MathsTools.hpp"

class World;
class SphereRigidBody;
class BoxRigidBody;
class PlaneRigidBody;

struct NewtonColliderDestroyer{
	void operator()(NewtonCollision* collider){
		NewtonDestroyCollision(collider);
	}
};

struct NewtonBodyDestroyer{
	void operator()(NewtonBody* body){
		NewtonDestroyBody(body);
	}
};

class RigidBody : public IRigidBody{

	public:
		RigidBody(const SphereRigidBody& sphere, World& world);
		RigidBody(const BoxRigidBody& box, World& world);
		RigidBody(const PlaneRigidBody& plane, World& world);

		~RigidBody();

		glm::mat4 getModelMatrix() const;

		glm::vec3 getVelocity() const;

		void setFriction(float staticFriction, float dynamicFriction);

		void applyRotation(const glm::vec3& axis, float angleRads);

		std::unique_ptr<NewtonBody, NewtonBodyDestroyer> body;

	private:
		std::unique_ptr<NewtonCollision, NewtonColliderDestroyer> collider;

		World& world;

		static int idPool;
		int id;

};

#endif
