#ifndef BULLET_RIGID_BODY_HPP
#define BULLET_RIGID_BODY_HPP

#include <iostream>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../BackEndFlags.hpp"
#include "../IRigidBody.hpp"
#include "../RigidBody.hpp"
#include "../../tools/MathsTools.hpp"

class World;
class SphereRigidBody;
class PlaneRigidBody;
class BoxRigidBody;

class RigidBody : public IRigidBody{

	public:
		RigidBody(const SphereRigidBody& sphere, World& world);
		RigidBody(const BoxRigidBody& box, World& world);
		RigidBody(const PlaneRigidBody& plane, World& world);

		~RigidBody();

		glm::mat4 getModelMatrix() const;

		glm::vec3 getVelocity() const;

		void setFriction(float staticFriction, float dynamicFriction);

		void applyRotation(const glm::vec3& axis, float angleRads);

		std::unique_ptr<btRigidBody> rigidBody;
		std::unique_ptr<btCollisionShape> collisionShape;
		World& world;
		bool stationary;

};

#endif
