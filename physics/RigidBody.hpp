#ifndef RIGID_BODY_HPP
#define RIGID_BODY_HPP

#include "BackEndFlags.hpp"

#ifdef USE_BULLET_BACKEND
#include "rigidbody/BulletRigidBody.hpp"
#elif USE_ODE_BACKEND
#include "rigidbody/ODERigidBody.hpp"
#elif USE_NEWTON_BACKEND
#include "rigidbody/NewtonRigidBody.hpp"
#elif USE_PHYSX_BACKEND
#include "rigidbody/PhysXRigidBody.hpp"
#elif USE_HAVOK_BACKEND
#include "rigidbody/HavokRigidBody.hpp"
#endif

class SphereRigidBody{
	public:
		SphereRigidBody(float radius, float xPos, float yPos, float zPos, float mass, float restitution) :
			radius(radius), xPos(xPos), yPos(yPos), zPos(zPos), mass(mass), restitution(restitution){};

		float radius;
		float xPos;
		float yPos;
		float zPos;
		float mass;
		float restitution;
};

class BoxRigidBody{
	public:
		BoxRigidBody(float width, float height, float depth,
					float xPos, float yPos, float zPos,
					float mass, float restitution, bool stationary):
			width(width),
			height(height),
			depth(depth),
			xPos(xPos),
			yPos(yPos),
			zPos(zPos),
			mass(mass),
			restitution(restitution),
			stationary(stationary)
		{};

		float width;
		float height;
		float depth;
		float xPos;
		float yPos;
		float zPos;
		float mass;
		float restitution;
		bool stationary;
};

class PlaneRigidBody{
	public:
		PlaneRigidBody(float height, float normalX, float normalY, float normalZ, float mass, float restitution) :
			height(height), normalX(normalX), normalY(normalY), normalZ(normalZ), mass(mass), restitution(restitution){};

		float height;
		float normalX;
		float normalY;
		float normalZ;
		float mass;
		float restitution;
};

#endif
