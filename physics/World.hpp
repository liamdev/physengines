#ifndef WORLD_HPP
#define WORLD_HPP

#include "BackEndFlags.hpp"

#ifdef USE_BULLET_BACKEND
#include "world/BulletWorld.hpp"
#elif USE_ODE_BACKEND
#include "world/ODEWorld.hpp"
#elif USE_NEWTON_BACKEND
#include "world/NewtonWorld.hpp"
#elif USE_PHYSX_BACKEND
#include "world/PhysXWorld.hpp"
#elif USE_HAVOK_BACKEND
#include "world/HavokWorld.hpp"
#endif

#endif
