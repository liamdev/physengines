#include "ODEWorld.hpp"
#include "../rigidbody/ODERigidBody.hpp"

World::World(){
	dInitODE();
	world = dWorldCreate();
	space = dHashSpaceCreate(0);
	dWorldSetGravity(world, 0, -9.81, 0);
	dWorldSetCFM(world, 0.00001);
	contactGroup = dJointGroupCreate(0);
};

World::~World(){
	dSpaceDestroy(space);
	dWorldDestroy(world);
	dCloseODE();
};

void World::update(float timeStep, unsigned int numIterations){
	// Inefficient pair-wise comparisons, but will do for the current purposes.
	dContact contacts[NUM_CONTACTS];
	for(unsigned int i = 0; i < geometries.size(); ++i){
		for(unsigned int j = i + 1; j < geometries.size(); ++j){
			int numCollisions = dCollide(geometries[i], geometries[j], NUM_CONTACTS, &contacts[0].geom, sizeof(dContact));

			for(int k = 0; k < numCollisions; ++k){
				float mu = 100000;
				if(dGeomGetData(geometries[i]) != nullptr){
					float* f = static_cast<float*>(dGeomGetData(geometries[i]));
					mu = f[0];
				}
				if(dGeomGetData(geometries[j]) != nullptr){
					float* f = static_cast<float*>(dGeomGetData(geometries[j]));
					if(f[0] > mu)
						mu = f[0];
				}
				contacts[k].surface.mode = dContactBounce;
				contacts[k].surface.mu = mu;
				contacts[k].surface.bounce = 0.5;
				contacts[k].surface.bounce_vel = 1.0;
				dJointID contact = dJointCreateContact(world, contactGroup, &contacts[k]);
				dJointAttach(contact, dGeomGetBody(contacts[k].geom.g1), dGeomGetBody(contacts[k].geom.g2));
			}
		}
	}

	dWorldSetQuickStepNumIterations(world, numIterations);
	dWorldQuickStep(world, timeStep);
	dJointGroupEmpty(contactGroup);
};

void World::addRigidBody(RigidBody* rigidBody){
	dGeomID rbID = rigidBody->geomID; 
	if(std::find(geometries.begin(), geometries.end(), rbID) == geometries.end()){
		geometries.push_back(rbID);
	}
}

void World::removeRigidBody(RigidBody* rigidBody){
	geometries.erase(std::remove(geometries.begin(), geometries.end(), rigidBody->geomID), geometries.end());
}
