#include "PhysXWorld.hpp"
#include "../rigidbody/PhysXRigidBody.hpp"

World::World() : pxAlloc(physx::PxDefaultAllocator()), pxErr(physx::PxDefaultErrorCallback()){
	foundation = PxCreateFoundation(PX_PHYSICS_VERSION, pxAlloc, pxErr);
	world = PxCreatePhysics(PX_PHYSICS_VERSION, *foundation, physx::PxTolerancesScale());

	dispatcher = physx::PxDefaultCpuDispatcherCreate(2);

	physx::PxSceneDesc sceneDesc(world->getTolerancesScale());
	sceneDesc.gravity = physx::PxVec3(0, -9.81f, 0);
	sceneDesc.cpuDispatcher = dispatcher;
	sceneDesc.filterShader = physx::PxDefaultSimulationFilterShader;
	scene = world->createScene(sceneDesc);
};

World::~World(){
	scene->release();
	dispatcher->release();
	world->release();
	foundation->release();
};

void World::update(float timeStep, unsigned int numIterations){
	scene->simulate(timeStep);
	scene->fetchResults(true);
};

void World::addRigidBody(RigidBody* rigidBody){
	if(rigidBody->isStaticActor()){
		scene->addActor(*(rigidBody->staticActor));
	} else {
		scene->addActor(*(rigidBody->dynamicActor));
	}
}

void World::removeRigidBody(RigidBody* rigidBody){
}
