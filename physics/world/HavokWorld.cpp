#include "HavokWorld.hpp"
#include "../rigidbody/HavokRigidBody.hpp"

// Havok CPP includes
#include <Common/Base/System/Init/PlatformInit.cxx>
#include <Common/Base/keycode.cxx>
#include <Common/Base/Config/hkProductFeatures.cxx>

static void HK_CALL errorHandler(const char* msg, void* userContext){
	std::cerr << msg << "\n";	
};

World::World(){
	// Memory allocation initialisation
	_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
	memoryRouter = hkMemoryInitUtil::initDefault(hkMallocAllocator::m_defaultMallocAllocator, hkMemorySystem::FrameInfo(500 * 1024));
	hkBaseSystem::init(memoryRouter, errorHandler);

	// World setup.
	hkpWorldCinfo worldInfo;
	worldInfo.setupSolverInfo(hkpWorldCinfo::SolverType::SOLVER_TYPE_8ITERS_MEDIUM);
	worldInfo.m_gravity = hkVector4(0, -9.81f, 0, 0);
	worldInfo.m_collisionTolerance = 0.01f;
	world = new hkpWorld(worldInfo);
	world->setGravity(hkVector4(0, -9.81f, 0, 0));
	hkpAgentRegisterUtil::registerAllAgents(world->getCollisionDispatcher());
};

World::~World(){
	world->removeReference();
	hkBaseSystem::quit();
	hkMemoryInitUtil::quit();
};

void World::update(float timeStep, unsigned int numIterations){
	world->stepDeltaTime(timeStep);
};

void World::addRigidBody(RigidBody* rigidBody){
	world->addEntity(rigidBody->body);
};

void World::removeRigidBody(RigidBody* rigidBody){
	world->removeEntity(rigidBody->body);
};
