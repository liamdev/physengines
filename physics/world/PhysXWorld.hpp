#ifndef PHYSX_WORLD_HPP
#define PHYSX_WORLD_HPP

#include "../BackEndFlags.hpp"

class RigidBody;

class World{

	public:
		World();
		~World();

		void update(float timeStep, unsigned int numIterations);

		void addRigidBody(RigidBody* body);
		void removeRigidBody(RigidBody* body);

		physx::PxPhysics* getWorld() const {return world;};

	private:
		physx::PxFoundation* foundation;
		physx::PxPhysics* world;
		physx::PxScene* scene;
		physx::PxDefaultCpuDispatcher* dispatcher;

		physx::PxDefaultAllocator pxAlloc;
		physx::PxDefaultErrorCallback pxErr;

};

#endif
