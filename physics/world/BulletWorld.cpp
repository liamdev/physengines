#include "BulletWorld.hpp"

World::World():
	broadphase(nullptr),
	collisionConfig(nullptr),
	collisionDispatcher(nullptr),
	solver(nullptr),
	dynamicsWorld(nullptr)
{
	broadphase = new btDbvtBroadphase();
	collisionConfig = new btDefaultCollisionConfiguration();
	collisionDispatcher = new btCollisionDispatcher(collisionConfig);
	solver = new btSequentialImpulseConstraintSolver();
	dynamicsWorld = new btDiscreteDynamicsWorld(collisionDispatcher, broadphase, solver, collisionConfig);
	dynamicsWorld->setGravity(btVector3(0, -9.8f, 0));
};

World::~World(){
	delete dynamicsWorld;
	delete solver;
	delete collisionDispatcher;
	delete collisionConfig;
	delete broadphase;
};

void World::update(float timeStep, unsigned int numIterations){
	dynamicsWorld->stepSimulation(timeStep, numIterations);
};

void World::addRigidBody(RigidBody* rigidBody){
	dynamicsWorld->addRigidBody(rigidBody->rigidBody.get());
}

void World::removeRigidBody(RigidBody* rigidBody){
	dynamicsWorld->removeRigidBody(rigidBody->rigidBody.get());
}
