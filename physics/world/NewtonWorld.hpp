#ifndef NEWTON_WORLD_HPP
#define NEWTON_WORLD_HPP

#include "../BackEndFlags.hpp"

#include "../RigidBody.hpp"

class RigidBody;

struct NewtonWorldDestroyer{
	void operator()(NewtonWorld* world){
		NewtonDestroy(world);
	}
};

class World{

	public:
		World();
		~World();

		void update(float timeStep, unsigned int numIterations);

		void addRigidBody(RigidBody* body);
		void removeRigidBody(RigidBody* body);

		NewtonWorld* getWorld(){return world.get();};

	private:
		std::unique_ptr<NewtonWorld, NewtonWorldDestroyer> world;

};

#endif
