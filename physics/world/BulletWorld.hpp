#ifndef BULLET_WORLD_HPP
#define BULLET_WORLD_HPP

#include "../BackEndFlags.hpp"

#include "../RigidBody.hpp"

class World{

	public:
		World();
		~World();

		void update(float timeStep, unsigned int numIterations);

		void addRigidBody(RigidBody* body);
		void removeRigidBody(RigidBody* body);

		btDiscreteDynamicsWorld* getWorld(){return dynamicsWorld;};

	private:

		btBroadphaseInterface* broadphase;
		btDefaultCollisionConfiguration* collisionConfig;
		btCollisionDispatcher* collisionDispatcher;
		btSequentialImpulseConstraintSolver* solver;
		btDiscreteDynamicsWorld* dynamicsWorld;

};

#endif
