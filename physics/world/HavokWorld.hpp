#ifndef HAVOK_WORLD_HPP
#define HAVOK_WORLD_HPP

#include "../BackEndFlags.hpp"

#include <Common/Base/Memory/System/Util/hkMemoryInitUtil.h>
#include <Physics2012/Dynamics/World/hkpWorld.h>
#include <Physics2012/Collide/Dispatch/hkpAgentRegisterUtil.h>

class RigidBody;

class World{

	public:
		World();
		~World();

		void update(float timeStep, unsigned int numIterations);

		void addRigidBody(RigidBody* body);
		void removeRigidBody(RigidBody* body);

	private:
		hkMemoryRouter* memoryRouter;
		hkpWorld* world;

};

#endif
