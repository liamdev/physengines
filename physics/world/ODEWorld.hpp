#ifndef ODE_WORLD_HPP
#define ODE_WORLD_HPP

#include <algorithm>
#include <vector>

#include "../BackEndFlags.hpp"

class RigidBody;

class World{

	public:
		World();
		~World();

		void update(float timeStep, unsigned int numIterations);

		void addRigidBody(RigidBody* body);
		void removeRigidBody(RigidBody* body);

		dWorldID getWorld() const {return world;};
		dSpaceID getSpace() const {return space;};

		static void nearCallback(void* data, dGeomID id1, dGeomID id2);

	private:
		static const int NUM_CONTACTS = 3;

		dSpaceID space;
		dWorldID world;
		dJointGroupID contactGroup;
		std::vector<dGeomID> geometries;

};

#endif
