#include "NewtonWorld.hpp"

World::World(){
	world = std::unique_ptr<NewtonWorld, NewtonWorldDestroyer>(NewtonCreate());
	NewtonSetSolverModel(world.get(), 1);
	NewtonMaterialSetDefaultElasticity(world.get(), 0, 0, 0.5f);
};

World::~World(){
};

void World::update(float timeStep, unsigned int numIterations){
	NewtonUpdate(world.get(), timeStep);
};

void forceTorqueCallback(const NewtonBody* body, float timestep, int threadIndex){
	float mass = 0, ix = 0, iy = 0, iz = 0;
	NewtonBodyGetMassMatrix(body, &mass, &ix, &iy, &iz);
	glm::vec3 force(0, -9.81 * mass, 0);
	NewtonBodyAddForce(body, &force[0]);
}

void World::addRigidBody(RigidBody* rigidBody){
	NewtonBodySetForceAndTorqueCallback(rigidBody->body.get(), forceTorqueCallback);
}

void World::removeRigidBody(RigidBody* rigidBody){
}
