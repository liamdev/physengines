#ifndef INPUT_HANDLER_HPP
#define INPUT_HANDLER_HPP

#include <utility>

#include <GLFW/glfw3.h>

class InputHandler{

	public:
		InputHandler();

		void keyEvent(GLFWwindow* window,
					int keycode, int scanCode,
					int glfwAction, int modifiers);

		void mousePosEvent(GLFWwindow* window, double xPos, double yPos);

		// Retrieves accumulated mouse movement since the last call to this function.
		std::pair<double, double> getMouseMovement();

		bool isKeyDown(int glfwKeyCode);

	private:
		static const unsigned int NUMBER_OF_KEYS = 500;

		bool keysPressed[NUMBER_OF_KEYS];

		double xPrevPos;
		double yPrevPos;
		double xMoveAccumulator;
		double yMoveAccumulator;

};

class Input{

	public:
		static void inputCallback(GLFWwindow* window,
						int keyCode, int scanCode,
						int glfwAction, int modifiers){
			Input::inputHandler.keyEvent(window, keyCode, scanCode, glfwAction, modifiers);
		};

		static void mousePosCallback(GLFWwindow* window, double xPos, double yPos){
			Input::inputHandler.mousePosEvent(window, xPos, yPos);
		}

		static InputHandler& getHandler(){
			return Input::inputHandler;
		}

		static void setHandler(InputHandler& inputHandler){
			Input::inputHandler = inputHandler;
		};

	private:
		static InputHandler& inputHandler;
};


#endif