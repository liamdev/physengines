#include "InputHandler.hpp"

// Default initial input handler.
InputHandler defaultInputHandler = InputHandler();
InputHandler& Input::inputHandler = defaultInputHandler;

InputHandler::InputHandler() : xPrevPos(-1), yPrevPos(-1), xMoveAccumulator(0), yMoveAccumulator(0){
	for(unsigned int i = 0; i < NUMBER_OF_KEYS; ++i)
		keysPressed[i] = false;
}

bool InputHandler::isKeyDown(int glfwKeyCode){
	return keysPressed[glfwKeyCode];
}

void InputHandler::keyEvent(GLFWwindow* window,
							int keyCode, int scanCode,
							int glfwAction, int modifiers){
	if(keyCode == GLFW_KEY_ESCAPE){
		glfwSetWindowShouldClose(window, true);
	}

	if(glfwAction == GLFW_RELEASE){
		keysPressed[keyCode] = false;
	}

	if(glfwAction == GLFW_PRESS){
		keysPressed[keyCode] = true;
	}
};

void InputHandler::mousePosEvent(GLFWwindow* window, double xPos, double yPos){
	if(xPrevPos == -1 && yPrevPos == -1){
		xPrevPos = xPos;
		yPrevPos = yPos;
		return;
	}

	xMoveAccumulator += xPos - xPrevPos;
	yMoveAccumulator += yPos - yPrevPos;
	xPrevPos = xPos;
	yPrevPos = yPos;
};

std::pair<double, double> InputHandler::getMouseMovement(){
	std::pair<double, double> movement(xMoveAccumulator, yMoveAccumulator);
	xMoveAccumulator = 0;	
	yMoveAccumulator = 0;
	return movement;
};
