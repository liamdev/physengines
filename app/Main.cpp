#include "Main.hpp"

int main(int argc, char** argv){
	
	// ===================
	// ARGUMENT PROCESSING 
	// ===================
	int testNo = 1;

	switch(argc){
		case 1:
			break;
		case 3:
			if(strcmp(argv[1], "-n") == 0){
				int test = std::atoi(argv[2]);
				if(test > 0 && test <= TESTS_AVAILABLE){
					testNo = test;
					break;
				} else {
					std::cout << "Test number must be between 1 and " << TESTS_AVAILABLE << ".\n";
					return -1;
				}
			} 
		default:
			std::cout << "Usage: run [-n number]\n";
			return -1;
	};

	// ===================
	// LOGGING SETUP
	// ===================
	Logger::startLogger(FilePaths::LOG_DIR);

	std::stringstream fName;
	fName << FilePaths::LOG_DIR << FilePaths::RESULTS_PREFIX << testNo << "-" << PHYS_ENGINE_NAME << FilePaths::RESULTS_EXT;
	std::ofstream resultsFile(fName.str());
	if(!resultsFile.is_open()){
		std::cout << "File at " << fName.str() << " could not be opened.\nResults will not be logged.\n";
	}

	// ===================
	// GLFW SETUP
	// ===================
	GLFWwindow* window = initGLFWWithWindow(WindowConfig::WIDTH, WindowConfig::HEIGHT,
											WindowConfig::TITLE, WindowConfig::RESIZABLE);
	if(!window){
		Logger::error("Failed to initialise a GLFW window.\n");
		return -1;
	}

	setGLFWInputHandler(window, Input::getHandler());
	Logger::log("GLFW setup complete.");

	// ===================
	// GLEW SETUP
	// ===================
	glewExperimental = true; // TODO: Investigate why glGenVertexArrays gives access violations without this.
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.8f, 0.8f, 0.8f, 1.0f);

	if(glewInit() != GLEW_OK){
		std::cerr << "Failed to initialise GLEW.\n";
		Logger::error("Failed to initialise GLEW.\n");
		return -1;
	}

	Logger::log("GLEW setup complete.\n");

	// ===================
	// OpenGL SETUP
	// ===================
	glEnable(GL_CULL_FACE);

	// ===================
	// WORLD SETUP
	// ===================
	SceneManager sceneManager;
	setUpTest(testNo, sceneManager);

	// ===================
	// UPDATE LOOP
	// ===================
	double prevFrame = glfwGetTime();

	while(!glfwWindowShouldClose(window)){
		double frameTime = glfwGetTime() - prevFrame;
		prevFrame += frameTime;

		manipulateScene(testNo, sceneManager, sceneManager.getTimeStep());
		sceneManager.update(frameTime);
		logResults(testNo, sceneManager, resultsFile);
		sceneManager.render();
	
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	closeGLFWWithWindow(window);
	return 0;
};
