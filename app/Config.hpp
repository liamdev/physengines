#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <string>

namespace WindowConfig{

	extern const int WIDTH;
	extern const int HEIGHT;
	extern const char* TITLE;
	extern const bool RESIZABLE;

};

namespace FilePaths{

	// Shaders
	extern const std::string SHADER_DIR;
	extern const std::string BASIC_VS;
	extern const std::string BASIC_FS;

	// Logging
	extern const std::string LOG_DIR;

	// Results
	extern const std::string RESULTS_PREFIX;
	extern const std::string RESULTS_EXT;

};

namespace ShaderNames{

	extern const std::string BASIC_SHADER;

};

namespace MeshNames{

	 extern const std::string SPHERE_MESH;
	 extern const std::string TRIANGLE_MESH;
	 extern const std::string PLANE_MESH;
	 extern const std::string BOX_MESH;

};

namespace UniformNames{
	extern const std::string MVP_UNIFORM;
	extern const std::string MODEL_UNIFORM;
};

namespace Maths{
	extern const float PI;
	extern const float RADS_TO_DEG;
};

namespace CameraParams{
	extern const float BOOST_MULTIPLIER;
	extern const float FOV_DEG;
	extern const float ASPECT_RATIO;
	extern const float Z_NEAR;
	extern const float Z_FAR;
};

#endif
