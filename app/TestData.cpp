#include "TestData.hpp"

bool TestData::getBool(const std::string& key) const{
	auto iter = boolData.find(key);
	if(iter == boolData.end()){
		std::stringstream ss;
		ss << "Requested user data not found for key: " << key << "\n";
		std::cerr << ss.str();
		Logger::error(ss.str());
		return false;
	} else {
		return iter->second;
	}
};

float TestData::getFloat(const std::string& key) const{
	auto iter = floatData.find(key);
	if(iter == floatData.end()){
		std::stringstream ss;
		ss << "Requested user data not found for key: " << key << "\n";
		std::cerr << ss.str();
		Logger::error(ss.str());
		return 0;
	} else {
		return iter->second;
	}
};
