#ifndef TEST_DATA_HPP
#define TEST_DATA_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>

#include "../tools/Logger.hpp"

class TestData{

	public:
		bool getBool(const std::string& key) const;
		float getFloat(const std::string& key) const;

		void putBool(const std::string& key, bool value){
			boolData[key] = value;	
		}

		void putFloat(const std::string& key, float value){
			floatData[key] = value;
		}

	private:
		std::unordered_map<std::string, bool> boolData;
		std::unordered_map<std::string, float> floatData;


};

#endif