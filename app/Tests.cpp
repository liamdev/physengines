#include "Tests.hpp"

void setUpTest(int testNo, SceneManager& sceneManager){
	switch(testNo){
		case 2:
			setUpTest2(sceneManager);
			break;
		case 3:
			setUpTest3(sceneManager);
			break;
		case 4:
			setUpTest4(sceneManager);
			break;
		case 5:
			setUpTest5(sceneManager);
			break;
		case 6:
			setUpTest6(sceneManager);
			break;
		default:
			setUpTest1(sceneManager);
			break;
	}
};

void manipulateScene(int testNo, SceneManager& sceneManager, float timeStep){
	switch(testNo){
		case 5:
			manipulateScene5(sceneManager, timeStep);
			break;
		default:
			break;
	}
};

void logResults(int testNo, SceneManager& sceneManager, std::ofstream& resultsFile){
	switch(testNo){
		case 1:
			logResults1(sceneManager, resultsFile);
			break;
		case 2:
			logResults2(sceneManager, resultsFile);
			break;
		case 5:
			logResults5(sceneManager, resultsFile);
			break;
		case 6:
			logResults6(sceneManager, resultsFile);
			break;
		default:
			break;
	}
};

void setUpTest1(SceneManager& sceneManager){
	sceneManager.setPhysicsStep(0.01f, 10);
	sceneManager.loadShader(ShaderNames::BASIC_SHADER, FilePaths::BASIC_VS, FilePaths::BASIC_FS);
	sceneManager.loadMesh(MeshNames::SPHERE_MESH, ShapeMeshType::SPHERE);
	sceneManager.loadMesh(MeshNames::PLANE_MESH, ShapeMeshType::PLANE);
	sceneManager.loadMesh(MeshNames::BOX_MESH, ShapeMeshType::BOX);
	sceneManager.getGlobalUniforms(ShaderNames::BASIC_SHADER).addVec3Uniform("lightPos", glm::vec3(-2, 5, 3));

	sceneManager.getCamera().setPositionRotation(glm::vec3(0, 2, 6), glm::vec3(0, 0, -1));
	
	Entity* ball = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball->setRigidBody(new RigidBody(SphereRigidBody(1, 0, 5, 0, 1, 0.5f), sceneManager.getWorld()));

	Entity* box = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::BOX_MESH);
	box->setRigidBody(new RigidBody(BoxRigidBody(1, 1, 1, 0, 0.5f, 0, 1, 1.0f, true), sceneManager.getWorld()));

	Entity* floor = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	floor->setRigidBody(new RigidBody(PlaneRigidBody(0, 0, 1, 0, 0, 1.0f), sceneManager.getWorld()));
};

void setUpTest2(SceneManager& sceneManager){
	sceneManager.setPhysicsStep(0.01f, 10);
	sceneManager.loadShader(ShaderNames::BASIC_SHADER, FilePaths::BASIC_VS, FilePaths::BASIC_FS);
	sceneManager.loadMesh(MeshNames::SPHERE_MESH, ShapeMeshType::SPHERE);
	sceneManager.loadMesh(MeshNames::PLANE_MESH, ShapeMeshType::PLANE);
	sceneManager.getGlobalUniforms(ShaderNames::BASIC_SHADER).addVec3Uniform("lightPos", glm::vec3(-2, 5, 3));

	sceneManager.getCamera().setPositionRotation(glm::vec3(0, 0, 6), glm::vec3(0, 0, -1));
	
	Entity* ball = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball->setRigidBody(new RigidBody(SphereRigidBody(1, 0, 0, 0, 1, 0.8f), sceneManager.getWorld()));
};

void setUpTest3(SceneManager& sceneManager){
	sceneManager.setPhysicsStep(0.01f, 10);
	sceneManager.loadShader(ShaderNames::BASIC_SHADER, FilePaths::BASIC_VS, FilePaths::BASIC_FS);
	sceneManager.loadMesh(MeshNames::SPHERE_MESH, ShapeMeshType::SPHERE);
	sceneManager.loadMesh(MeshNames::PLANE_MESH, ShapeMeshType::PLANE);
	sceneManager.getGlobalUniforms(ShaderNames::BASIC_SHADER).addVec3Uniform("lightPos", glm::vec3(-2, 5, 3));

	sceneManager.getCamera().setPositionRotation(glm::vec3(0, 2, 6), glm::vec3(0, 0, -1));
	
	Entity* ball1 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball1->setRigidBody(new RigidBody(SphereRigidBody(1, 0, 1, 0, 1, 0.5f), sceneManager.getWorld()));
	Entity* ball2 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball2->setRigidBody(new RigidBody(SphereRigidBody(1, 0, 3.1f, 0, 1, 0.5f), sceneManager.getWorld()));
	Entity* ball3 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball3->setRigidBody(new RigidBody(SphereRigidBody(1, 0, 5.2f, 0, 1, 0.5f), sceneManager.getWorld()));

	Entity* floor = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	floor->setRigidBody(new RigidBody(PlaneRigidBody(0, 0, 1, 0, 0, 1.0f), sceneManager.getWorld()));
};

void setUpTest4(SceneManager& sceneManager){
	sceneManager.setPhysicsStep(0.01f, 10);
	sceneManager.loadShader(ShaderNames::BASIC_SHADER, FilePaths::BASIC_VS, FilePaths::BASIC_FS);
	sceneManager.loadMesh(MeshNames::SPHERE_MESH, ShapeMeshType::SPHERE);
	sceneManager.loadMesh(MeshNames::BOX_MESH, ShapeMeshType::BOX);
	sceneManager.loadMesh(MeshNames::PLANE_MESH, ShapeMeshType::PLANE);
	sceneManager.getGlobalUniforms(ShaderNames::BASIC_SHADER).addVec3Uniform("lightPos", glm::vec3(-2, 5, 3));

	sceneManager.getCamera().setPositionRotation(glm::vec3(0, 2, 6), glm::vec3(0, 0, -1));

	Entity* box1 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::BOX_MESH);
	box1->setRigidBody(new RigidBody(BoxRigidBody(1, 1, 1, 0, 0.8f, 0, 1, 0.5f, false), sceneManager.getWorld()));
	Entity* box2 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::BOX_MESH);
	box2->setRigidBody(new RigidBody(BoxRigidBody(1, 1, 1, 0, 2.6f, 0, 1, 0.5f, false), sceneManager.getWorld()));
	Entity* box3 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::BOX_MESH);
	box3->setRigidBody(new RigidBody(BoxRigidBody(1, 1, 1, 0, 4.4f, 0, 1, 0.5f, false), sceneManager.getWorld()));

	Entity* floor = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	floor->setRigidBody(new RigidBody(PlaneRigidBody(0, 0, 1, 0, 0, 1.0f), sceneManager.getWorld()));
};

void setUpTest5(SceneManager& sceneManager){
	sceneManager.setPhysicsStep(0.01f, 10);
	sceneManager.loadShader(ShaderNames::BASIC_SHADER, FilePaths::BASIC_VS, FilePaths::BASIC_FS);
	sceneManager.loadMesh(MeshNames::BOX_MESH, ShapeMeshType::BOX);
	sceneManager.loadMesh(MeshNames::PLANE_MESH, ShapeMeshType::PLANE);
	sceneManager.getGlobalUniforms(ShaderNames::BASIC_SHADER).addVec3Uniform("lightPos", glm::vec3(-2, 5, 3));

	sceneManager.getCamera().setPositionRotation(glm::vec3(0, 2, 6), glm::vec3(0, 0, -1));

	Entity* box1 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::BOX_MESH);
	box1->setRigidBody(new RigidBody(BoxRigidBody(1, 1, 1, 0, 0.5f, 0, 1, 0.5f, false), sceneManager.getWorld()));
	box1->getRigidBody()->setFriction(0.5f, 0.5f);

	Entity* floor = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	floor->setRigidBody(new RigidBody(PlaneRigidBody(0, 0, 1, 0, 0, 1.0f), sceneManager.getWorld()));
	floor->getRigidBody()->setFriction(0.1f, 0.1f);

	sceneManager.getTestData().putFloat(TDATA_ELAPSED_TIME, 0.0f);
	sceneManager.getTestData().putBool(TDATA_LOGGING_COMPLETE, false);
	sceneManager.getTestData().putFloat(TDATA_RESULT, 0.0f);
};

void manipulateScene5(SceneManager& sceneManager, float timeStep){
	bool complete = sceneManager.getTestData().getBool(TDATA_LOGGING_COMPLETE);
	if(complete){
		return;
	}

	float time = sceneManager.getTestData().getFloat(TDATA_ELAPSED_TIME);
	float elapsedTime = time + timeStep;

	if((int)(time) != (int)(elapsedTime)){ // Tick every 1s
		const Entity* box = sceneManager.getEntity(0);
		const Entity* floor = sceneManager.getEntity(1);

		glm::vec3 vel = box->getRigidBody()->getVelocity();
		float velSquared = getVecMagnitudeSquared(vel);

		if(elapsedTime > 1 && velSquared > 0.2f){ // Still moving, consider it sliding.
			sceneManager.getTestData().putFloat(TDATA_RESULT, ((int)elapsedTime) * 0.05f);
		} else {
			floor->getRigidBody()->applyRotation(glm::vec3(0, 0, 1), 0.05f);
			// Apply a small box rotation in order to 'wake up' the box in some engines.
			box->getRigidBody()->applyRotation(glm::vec3(0, 1, 0), 0.1f);
		}
	}

	sceneManager.getTestData().putFloat(TDATA_ELAPSED_TIME, elapsedTime);
};

void setUpTest6(SceneManager& sceneManager){
	sceneManager.setPhysicsStep(0.01f, 10);
	sceneManager.loadShader(ShaderNames::BASIC_SHADER, FilePaths::BASIC_VS, FilePaths::BASIC_FS);
	sceneManager.loadMesh(MeshNames::SPHERE_MESH, ShapeMeshType::SPHERE);
	sceneManager.loadMesh(MeshNames::PLANE_MESH, ShapeMeshType::PLANE);
	sceneManager.getGlobalUniforms(ShaderNames::BASIC_SHADER).addVec3Uniform("lightPos", glm::vec3(-2, 5, 3));

	sceneManager.getCamera().setPositionRotation(glm::vec3(0, 2, 6), glm::vec3(0, 0, -1));

	Entity* ball1 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball1->setRigidBody(new RigidBody(SphereRigidBody(1, -1, 4.0f, -1, 1, 0.5f), sceneManager.getWorld()));
	Entity* ball2 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball2->setRigidBody(new RigidBody(SphereRigidBody(1, 1, 4.0f, -1, 1, 0.5f), sceneManager.getWorld()));
	Entity* ball3 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball3->setRigidBody(new RigidBody(SphereRigidBody(1, -1, 4.0f, 1, 1, 0.5f), sceneManager.getWorld()));
	Entity* ball4 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::SPHERE_MESH);
	ball4->setRigidBody(new RigidBody(SphereRigidBody(1, 1, 4.0f, 1, 1, 0.5f), sceneManager.getWorld()));

	Entity* wall1 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	wall1->setRigidBody(new RigidBody(PlaneRigidBody(0, 1, 1, 0, 0, 1.0f), sceneManager.getWorld()));
	Entity* wall2 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	wall2->setRigidBody(new RigidBody(PlaneRigidBody(0, -1, 1, 0, 0, 1.0f), sceneManager.getWorld()));
	Entity* wall3 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	wall3->setRigidBody(new RigidBody(PlaneRigidBody(0, 0, 1, 1, 0, 1.0f), sceneManager.getWorld()));
	Entity* wall4 = sceneManager.createEntity(ShaderNames::BASIC_SHADER, MeshNames::PLANE_MESH);
	wall4->setRigidBody(new RigidBody(PlaneRigidBody(0, 0, 1, -1, 0, 1.0f), sceneManager.getWorld()));

	sceneManager.getTestData().putBool(TDATA_LOGGING_COMPLETE, false);
	sceneManager.getTestData().putFloat(TDATA_ELAPSED_TIME, 0);
};

void logResults1(SceneManager& sceneManager, std::ofstream& resultsFile){
	const Entity* e = sceneManager.getEntity(0);

	if(e == nullptr){
		std::cout << "Requested entity with ID 0, but it does not exist.\n";
		return;
	}

	glm::mat4 transform = e->getModelMatrix();

	resultsFile << transform[3][1] << "\n";

};

void logResults2(SceneManager& sceneManager, std::ofstream& resultsFile){
	const Entity* e = sceneManager.getEntity(0);

	if(e == nullptr){
		std::cout << "Requested entity with ID 0, but it does not exist.\n";
		return;
	}

	glm::mat4 transform = e->getModelMatrix();

	resultsFile << transform[3][1] << "\n";
};

void logResults5(SceneManager& sceneManager, std::ofstream& resultsFile){
	bool complete = sceneManager.getTestData().getBool(TDATA_LOGGING_COMPLETE);
	float result = sceneManager.getTestData().getFloat(TDATA_RESULT);
	if(complete){
		return;
	} else if(result > 0){
		std::stringstream ss;
		ss << result << "\n";
		resultsFile << ss.str();
		sceneManager.getTestData().putBool(TDATA_LOGGING_COMPLETE, true);
	}
};


void logResults6(SceneManager& sceneManager, std::ofstream& resultsFile){
	if(sceneManager.getTestData().getBool(TDATA_LOGGING_COMPLETE)){
		return;
	}

	const Entity* balls[4];
	float velocities[4];

	for(int i = 0; i < 4; ++i){
		balls[i] = sceneManager.getEntity(i);
		velocities[i] = getVecMagnitudeSquared(balls[i]->getRigidBody()->getVelocity());
	}

	if(velocities[0] < 0.001f && velocities[1] < 0.001f &&
		velocities[2] < 0.001f && velocities[3] < 0.001f){
		int restSteps = static_cast<int>(sceneManager.getTestData().getFloat(TDATA_ELAPSED_TIME));

		if(restSteps > 10){
			glm::vec3 leftNormal = glm::normalize(glm::vec3(1, 1, 0));
			glm::vec3 rightNormal = glm::normalize(glm::vec3(-1, 1, 0));
			glm::vec3 frontNormal = glm::normalize(glm::vec3(0, 1, -1));
			glm::vec3 backNormal = glm::normalize(glm::vec3(0, 1, 1));

			for(int i = 0; i < 4; ++i){
				float totalPen = 0;

				float xPos = balls[i]->getModelMatrix()[3][0];
				float yPos = balls[i]->getModelMatrix()[3][1];
				float zPos = balls[i]->getModelMatrix()[3][2];

				if(xPos < 0){
					totalPen += std::abs((leftNormal.x * xPos + leftNormal.y * yPos) - 1);
				} else {
					totalPen += std::abs((rightNormal.x * xPos + rightNormal.y * yPos) - 1);
				}
				if(zPos < 0){
					totalPen += std::abs((backNormal.y * yPos + backNormal.z * zPos) - 1);
				} else {
					totalPen += std::abs((frontNormal.y * yPos + frontNormal.z * zPos) - 1);
				}

				resultsFile << totalPen << "\n";
			}

			sceneManager.getTestData().putBool(TDATA_LOGGING_COMPLETE, true);
		} else {
			sceneManager.getTestData().putFloat(TDATA_ELAPSED_TIME, static_cast<float>(restSteps + 1));
		}
	} else {
		sceneManager.getTestData().putFloat(TDATA_ELAPSED_TIME, 0);
	}
};