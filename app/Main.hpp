#ifndef MAIN_HPP
#define MAIN_HPP

#include <cstring>
#include <fstream>
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Config.hpp"
#include "Tests.hpp"
#include "../input/InputHandler.hpp"
#include "../rendering/MeshLoader.hpp"
#include "../rendering/SceneManager.hpp"
#include "../rendering/Uniforms.hpp"
#include "../tools/GLFWTools.hpp"
#include "../tools/GLTools.hpp"
#include "../tools/Logger.hpp"

int main(int argc, char** argv);

#endif
