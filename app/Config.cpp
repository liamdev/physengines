#include "Config.hpp"

namespace WindowConfig{

	const int WIDTH = 800;
	const int HEIGHT = 600;
	const char* TITLE = "Physics Test";
	const bool RESIZABLE = false;

};

namespace FilePaths{

	// Shaders
	const std::string SHADER_DIR = "shaders/";
	const std::string BASIC_VS = SHADER_DIR + "basicvs.glsl";
	const std::string BASIC_FS = SHADER_DIR + "basicfs.glsl";

	// Logging
	const std::string LOG_DIR = "logs/";

	// Results
	const std::string RESULTS_PREFIX = "results";
	const std::string RESULTS_EXT = ".log";

};

namespace ShaderNames{

	const std::string BASIC_SHADER = "___basicShader";

};

namespace MeshNames{

	 const std::string SPHERE_MESH = "___sphereMesh";
	 const std::string TRIANGLE_MESH = "___triangleMesh";
	 const std::string PLANE_MESH = "___planeMesh";
	 const std::string BOX_MESH = "___boxMesh";

};

namespace UniformNames{
	const std::string MVP_UNIFORM = "mvp";
	const std::string MODEL_UNIFORM = "model";
};

namespace Maths{
	const float PI = 3.14159265f;
	const float RADS_TO_DEG = 180.0f / PI;
};

namespace CameraParams{
	const float BOOST_MULTIPLIER = 5;
	const float FOV_DEG = 60;
	const float ASPECT_RATIO = 4.0f / 3;
	const float Z_NEAR = 0.01f;
	const float Z_FAR = 10000.0f;
};
