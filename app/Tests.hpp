#ifndef PHYSICS_TESTS_HPP
#define PHYSICS_TESTS_HPP

#include <fstream>

#include "../rendering/SceneManager.hpp"
#include "../tools/MathsTools.hpp"

const int TESTS_AVAILABLE = 6;

const std::string TDATA_ELAPSED_TIME = "physicsTime";
const std::string TDATA_LOGGING_COMPLETE = "loggingComplete";
const std::string TDATA_RESULT = "testResult";

void setUpTest(int testNo, SceneManager& sceneManager);
void setUpTest1(SceneManager& sceneManager); // Ball bouncing on a plane.
void setUpTest2(SceneManager& sceneManager); // Ball in freefall.
void setUpTest3(SceneManager& sceneManager); // Stacked balls.
void setUpTest4(SceneManager& sceneManager); // Stacked boxes.
void setUpTest5(SceneManager& sceneManager); // Sliding box.
void setUpTest6(SceneManager& sceneManager); // Balls falling into pyramid.

void manipulateScene(int testNo, SceneManager& sceneManager, float timeStep);
void manipulateScene5(SceneManager& sceneManager, float timeStep); // Sliding box.

void logResults(int testNo, SceneManager& sceneManager, std::ofstream& resultsFile);
void logResults1(SceneManager& sceneManager, std::ofstream& resultsFile); // Ball bouncing on a plane.
void logResults2(SceneManager& sceneManager, std::ofstream& resultsFile); // Ball in freefall.
void logResults5(SceneManager& sceneManager, std::ofstream& resultsFile); // Sliding box.
void logResults6(SceneManager& sceneManager, std::ofstream& resultsFile); // Balls falling into pyramid.


#endif
