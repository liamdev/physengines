#version 330 core

in vec3 vertPosFrag;
in vec3 vertNormalFrag;
in vec3 lightPosFrag;
out vec4 fragColour;

void main(){
	vec3 lightDir = normalize(lightPosFrag - vertPosFrag);
	float amount = clamp(dot(vertNormalFrag, lightDir), 0, 1);
	fragColour = vec4(amount, amount, amount, 1.0f);
}
