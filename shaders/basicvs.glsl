#version 330 core

uniform mat4 model;
uniform mat4 mvp;
uniform vec3 lightPos;

layout (location = 0) in vec3 vertPos;
layout (location = 1) in vec3 vertNormal;

out vec3 vertPosFrag;
out vec3 lightPosFrag;
out vec3 vertNormalFrag;

void main(){
	gl_Position = mvp * vec4(vertPos, 1.0f);

	vertPosFrag = (model * vec4(vertPos, 1.0f)).xyz;

	lightPosFrag = lightPos;
	vertNormalFrag = normalize((model * vec4(vertNormal, 0.0f)).xyz);
}
