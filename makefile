CC=g++
CFLAGS=-Wall -std=c++11
OBJS=
SUBDIRS=app/ entity/ input/ physics/ rendering/ tools/
OBJ_DIR=bin/
OUT_OBJS=$(addprefix $(OBJ_DIR), $(OBJS))
BULLET_OBJ_DIR=$(addprefix $(OBJ_DIR), bullet/)
ODE_OBJ_DIR=$(addprefix $(OBJ_DIR), ode/)
NEWTON_OBJ_DIR=$(addprefix $(OBJ_DIR), newton/)
INC=-I /usr/include/bullet -I /usr/include/newton
LIBS=-lGL -lGLEW -lglfw -lBulletDynamics -lBulletCollision -lLinearMath -lode -lNewton
DEFS=

all: OUT=runbullet
all: CFLAGS+=-O3
all: $(OUT_OBJS)
	$(foreach subdir,$(SUBDIRS), make -C $(subdir) CFLAGS="$(CFLAGS)";)
	$(CC) $(CFLAGS) $(OBJ_DIR)*.o $(BULLET_OBJ_DIR)*.o $(LIBS) $(DEFS) -o $(OUT)

debug: OUT=runbullet
debug: CFLAGS+=-g
debug: $(OUT_OBJS)
	$(foreach subdir,$(SUBDIRS), make -C $(subdir) CFLAGS="$(CFLAGS)";)
	$(CC) $(CFLAGS) $(OBJ_DIR)*.o $(BULLET_OBJ_DIR)*.o $(LIBS) $(DEFS) -o $(OUT)

ode: OUT=runode
ode: CFLAGS+=-O3
ode: DEFS+=-DUSE_ODE_BACKEND
ode: $(OUT_OBJS)
	$(foreach subdir,$(SUBDIRS), make -C $(subdir) ode CFLAGS="$(CFLAGS)";)
	$(CC) $(CFLAGS) $(OBJ_DIR)*.o $(ODE_OBJ_DIR)*.o $(LIBS) $(DEFS) -o $(OUT)

oded: OUT=runode
oded: CFLAGS+=-g
oded: DEFS+=-DUSE_ODE_BACKEND
oded: $(OUT_OBJS)
	$(foreach subdir,$(SUBDIRS), make -C $(subdir) ode CFLAGS="$(CFLAGS)";)
	$(CC) $(CFLAGS) $(OBJ_DIR)*.o $(ODE_OBJ_DIR)*.o $(LIBS) $(DEFS) -o $(OUT)

newton: OUT=runnewton
newton: CFLAGS+=-O3
newton: DEFS+=-DUSE_NEWTON_BACKEND
newton: $(OUT_OBJS)
	$(foreach subdir, $(SUBDIRS), make -C $(subdir) newton CFLAGS="$(CFLAGS)";)
	$(CC) $(CFLAGS) $(OBJ_DIR)*.o $(NEWTON_OBJ_DIR)*.o $(LIBS) $(DEFS) -o $(OUT)

newtond: OUT=runnewton
newtond: CFLAGS+=-g
newtond: DEFS+=-DUSE_NEWTON_BACKEND
newtond: $(OUT_OBJS)
	$(foreach subdir, $(SUBDIRS), make -C $(subdir) newton CFLAGS="$(CFLAGS)";)
	$(CC) $(CFLAGS) $(OBJ_DIR)*.o $(NEWTON_OBJ_DIR)*.o $(LIBS) $(DEFS) -o $(OUT)

$(OBJ_DIR)%.o: %.cpp
	$(CC) $(CFLAGS) $(INC) $(DEFS) -c $< -o $@

$(BULLET_OBJ_DIR)%.o: %.cpp
	$(CC) $(CFLAGS) $(INC) $(DEFS) -c $< -o $@

$(ODE_OBJ_DIR)%.o: %.cpp
	$(CC) $(CFLAGS) $(INC) $(DEFS) -c $< -o $@

$(NEWTON_OBJ_DIR)%.o: %.cpp
	$(CC) $(CFLAGS) $(INC) $(DEFS) -c $< -o $@

clean:
	-rm $(OBJ_DIR)*.o
	-rm $(BULLET_OBJ_DIR)*.o
	-rm $(ODE_OBJ_DIR)*.o
	-rm $(NEWTON_OBJ_DIR)*.o
